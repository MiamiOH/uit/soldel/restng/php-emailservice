# Email Serivce RESTful Web Service

## Description

php-emailservice has been converted into RESTng 2.0 requirements. RESTng 2.0 conversion task focued on fixing syntax or directory structure to meet RESTng 2.0. No functionality or logic change were made. After conversion was done, PHPUnit has been executed. PostEmailTest failed PHPUnit test. No fix made. 

## API Documentation

API documentation can be found on swagger page: <ws_url>/api/swagger-ui/#/person

## Local Development Setup

1. pull down latest source code from this repository
2. install composer dependencies: `composer update`

## Testing

### Unit Testing

Unit test cases in this project is written using PHPUnit. 

`phpunit` should pass without any error message before and after making any change. Code coverage report will be
automatically generated after `phpunit` being ran and put into `test/coverage` folder.