<?php

/*
-----------------------------------------------------------
FILE NAME: postEmailTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Erin Mills

DESCRIPTION:
This php class is used to test the POST method of the email service.

ENVIRONMENT DEPENDENCIES:
RESTng Framework
PHPUnit
Person/Email Service

TABLE USAGE:

Web Service Usage:
	Person/Email service (POST)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/14/2015               MILLSE
Description:  Initial Draft

-----------------------------------------------------------
 */

namespace MiamiOH\PhpEmailService\Services;

use MiamiOH\RESTng\Legacy\DB\STH;
use MiamiOH\RESTng\Legacy\DB\STH\OCI8;
use MiamiOH\RESTng\Util\User;

define('DB_NO_ERROR', 0);

class PostEmailTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $email, $user, $dbh;
    private $userName = 'testUser';

    /**
     * This setUp method is run before every test method.
     * It sets up a mock API, DBH and DB object with methods that the email class uses
     * and injects those into the Email object.
     */
    protected function setUp()
    {

        //Create the mock API object and define its newResponse method
        $api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            //->setMethods(array('newResponse', 'callResource', 'locationFor'))
            ->setMethods(array('newResponse'))
            ->getMock();
        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'perform', 'prepare'))
            ->getMock();

        //set up the mock database object and its getHandle method
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();
        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock user
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized', 'getUsername'))
            ->getMock();
        $this->user->method('getUsername')->willReturn($this->userName);

        //Create the email object we will be unit testing and pass it the mock objects
        $this->email = new \MiamiOH\PhpEmailService\Services\Email();
        $this->email->setDatabase($db);
        $this->email->setApiUser($this->user);

    }

    public function testCreateEmailSuccess()
    {
        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();
        $request->expects($this->once())->method('getData')->willReturn($this->mockDataRecordSuccess);
        $this->email->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        $oci8 = $this->getMockBuilder(STH::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'bind_by_name',
                'execute'
            ])
            ->getMock();
        $oci8->method('bind_by_name')->willReturn(null);
        $oci8->method('execute')->willReturn(null);
        $this->dbh->method('prepare')->willReturn($oci8);

        /*** Call the getEmail and get the payload ***/
        $resp = $this->email->createEmail();
        $payload = $resp->getPayload();

        /*** Make assertions about the response from the creatEmail method ***/
        $this->assertEquals($payload[0]['pidm'], '9999436');
        $this->assertEquals($payload[0]['code'], '201');
        $this->assertEquals($payload[0]['message'], 'Email successfully inserted.');
    }

    public function testCreateEmailFailAlreadyExists()
    {
        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();
        $request->expects($this->once())->method('getData')->willReturn($this->mockDataRecordFail);
        $this->email->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        $oci8 = $this->getMockBuilder(STH::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'bind_by_name',
                'execute'
            ])
            ->getMock();
        $oci8->method('bind_by_name')->willReturn(null);
        $oci8->method('execute')->willThrowException(new \Exception('asdf'));
        $this->dbh->method('prepare')->willReturn($oci8);

        /*** Call the getEmail and get the payload ***/
        $resp = $this->email->createEmail();
        $payload = $resp->getPayload();

        /*** Make assertions about the response from the creatEmail method ***/
        $this->assertEquals($payload[0]['pidm'], '1147436');
        $this->assertEquals($payload[0]['code'], '500');
    }

    public function testCreateEmailFailInsertError()
    {
        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();
        $request->expects($this->once())->method('getData')->willReturn($this->mockDataRecordFail);
        $this->email->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        $oci8 = $this->getMockBuilder(STH::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'bind_by_name',
                'execute'
            ])
            ->getMock();
        $oci8->method('bind_by_name')->willReturn(null);
        $oci8->method('execute')->willThrowException(new \Exception('asdf'));
        $this->dbh->method('prepare')->willReturn($oci8);

        /*** Call the getEmail and get the payload ***/
        $resp = $this->email->createEmail();
        $payload = $resp->getPayload();

        /*** Make assertions about the response from the creatEmail method ***/
        $this->assertEquals($payload[0]['pidm'], '1147436');
        $this->assertEquals($payload[0]['code'], '500');
    }

    public function testCreateEmailCommentTooLong()
    {
        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();
        $request->expects($this->once())->method('getData')->willReturn($this->mockDataRecordLongComment);
        $this->email->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        $this->dbh->method('perform')->willReturn(false);
        $this->dbh->method('queryall_array')->will($this->returnCallback(array($this, 'queryallArrayResultsNull')));

        /*** Call the getEmail and get the payload ***/
        $resp = $this->email->createEmail();
        $payload = $resp->getPayload();

        /*** Make assertions about the response from the creatEmail method ***/
        $this->assertEquals($payload[0]['pidm'], '9999436');
        $this->assertEquals($payload[0]['code'], '500');
        $this->assertEquals($payload[0]['message'], 'The comment given was too long. Comments can only be 60 characters long.');
    }

    public function testCreateEmailPidmNotNumeric()
    {
        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();
        $request->expects($this->once())->method('getData')->willReturn($this->mockDataRecordPidmNotNumeric);
        $this->email->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        $this->dbh->method('perform')->willReturn(false);
        $this->dbh->method('queryall_array')->will($this->returnCallback(array($this, 'queryallArrayResultsNull')));

        /*** Call the getEmail and get the payload ***/
        $resp = $this->email->createEmail();
        $payload = $resp->getPayload();

        /*** Make assertions about the response from the creatEmail method ***/
        $this->assertEquals($payload[0]['pidm'], '9999436x');
        $this->assertEquals($payload[0]['code'], '500');
        $this->assertEquals($payload[0]['message'], 'Pidm must be numeric.');
    }

    public function testCreateEmailInvalidToken()
    {
        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $this->email->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            //->will($this->returnCallback(array($this, 'authorizedUser'))); //for an authorized user
            ->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        /*** Call the getEmail and get the payload ***/
        $resp = $this->email->createEmail();

        /*** Make assertions ***/
        $this->assertEquals(\MiamiOH\RESTng\App::API_UNAUTHORIZED, $resp->getStatus());
    }

    //helper methods
    public function authorizedUser()
    {
        return true;
    }

    public function notAuthorizedUser()
    {
        return false;
    }

    public function queryallArrayResults()
    {
        return "stuff";
    }

    public function queryallArrayResultsNull()
    {
        return null;
    }

    //private vars
    private $mockDataRecordSuccess =
        array( //array of 3 database records
            array( //record 1
                'pidm' => '9999436',
                'emailCode' => 'MAE',
                'emailAddress' => 'emailWSpostTesting@miamioh.edu',
                'statusInd' => 'A',
                'preferredInd' => 'N',
                'comment' => 'this is a test email address for the createEmail method.',
                'dispWebInd' => 'N'
            ),
        );
    private $mockDataRecordLongComment =
        array( //array of 3 database records
            array( //record 1
                'pidm' => '9999436',
                'emailCode' => 'MAE',
                'emailAddress' => 'emailWSpostTesting@miamioh.edu',
                'statusInd' => 'A',
                'preferredInd' => 'N',
                'comment' => 'this is a test email address for the email resource\'s createEmail method.',
                'dispWebInd' => 'N'
            ),
        );
    private $mockDataRecordPidmNotNumeric =
        array( //array of 3 database records
            array( //record 1
                'pidm' => '9999436x',
                'emailCode' => 'MAE',
                'emailAddress' => 'emailWSpostTesting@miamioh.edu',
                'statusInd' => 'A',
                'preferredInd' => 'N',
                'comment' => 'this is a test email address for the createEmail method.',
                'dispWebInd' => 'N'
            ),
        );
    private $mockDataRecordFail =
        array( //array of 3 database records
            array( //record 1
                'pidm' => '1147436',
                'emailCode' => 'ADM',
                'emailAddress' => 'testing@gmail.com',
                'statusInd' => 'A',
                'preferredInd' => 'N',
                'comment' => 'comment to add',
                'dispWebInd' => 'N'
            ),
        );
}