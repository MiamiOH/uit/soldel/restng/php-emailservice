<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2\Exceptions;


use MiamiOH\PhpEmailService\Tests\Unit\V2\TestCase;
use MiamiOH\PhpEmailService\V2\Exceptions\ApplicationException;

/**
 * @covers \MiamiOH\PhpEmailService\V2\Exceptions\ApplicationException
 */
class ApplicationExceptionTest extends TestCase
{
    public function testGetHierarchyErrorMessages()
    {
        $e = new ApplicationException('msg 1', 0,
            new ApplicationException('msg 2', 0,
                new ApplicationException('msg 3')));

        $this->assertSame([
            'msg 2',
            'msg 3',
        ], $e->getErrors());
        $this->assertSame('msg 1', $e->getMessage());
    }
}