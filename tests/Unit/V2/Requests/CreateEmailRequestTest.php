<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2\Requests;


use Illuminate\Validation\ValidationException;
use MiamiOH\PhpEmailService\Tests\Unit\V2\TestCase;
use MiamiOH\PhpEmailService\V2\Requests\CreateEmailRequest;

/**
 * @covers \MiamiOH\PhpEmailService\V2\Requests\CreateEmailRequest
 */
class CreateEmailRequestTest extends TestCase
{
    public function testFailedToCreateRequestDueToBadRequest()
    {
        $data = [
            [
                'typeCode' => 'ADM',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isForced' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'kaldf alkdsf lasd',
                'createdBy' => 'akdlfjs'
            ],
            [
                'pidm' => 111111,
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isForced' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'kaldf alkdsf lasd',
                'createdBy' => 'akdlfjs'
            ],
            [
                'pidm' => 111111,
                'typeCode' => 'ADM',
                'isPreferred' => true,
                'isForced' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'kaldf alkdsf lasd',
                'createdBy' => 'akdlfjs'
            ],
            [
                'pidm' => 111111,
                'typeCode' => 'ADM',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isForced' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'kaldf alkdsf lasd',
            ],
            [
                'pidm' => 'asdfasdf',
                'typeCode' => 'ADM',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isForced' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'kaldf alkdsf lasd',
                'createdBy' => 'akdlfjs'
            ],
            [
                'pidm' => 111111,
                'typeCode' => 'AADSS',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isForced' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'kaldf alkdsf lasd',
                'createdBy' => 'akdlfjs'
            ],
            [
                'pidm' => 111111,
                'typeCode' => 'ADM',
                'email' => 'aaa@gmail.com',
                'isPreferred' => 'asdfasd',
                'isForced' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'kaldf alkdsf lasd',
                'createdBy' => 'akdlfjs'
            ],
            [
                'pidm' => 111111,
                'typeCode' => 'ADM',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isForced' => 'asdfas',
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'kaldf alkdsf lasd',
                'createdBy' => 'akdlfjs'
            ],
            [
                'pidm' => 111111,
                'typeCode' => 'ADM',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isForced' => true,
                'isActive' => 'asdgasdf',
                'isDisplayedOnWeb' => true,
                'comment' => 'kaldf alkdsf lasd',
                'createdBy' => 'akdlfjs'
            ],
            [
                'pidm' => 111111,
                'typeCode' => 'ADM',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isForced' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => 'sdfgsfdg',
                'comment' => 'kaldf alkdsf lasd',
                'createdBy' => 'akdlfjs'
            ],
        ];

        foreach ($data as $d) {
            try {
                CreateEmailRequest::createFromArray($d);
                $this->fail();
            } catch (ValidationException $e) {
                //
            }
        }

        $this->assertTrue(true);
    }

    public function testCreateRequestFromRequestBody()
    {
        $this->assertSame([
            'pidm' => 111111,
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'isForced' => true,
            'comment' => 'kaldf alkdsf lasd',
            'createdBy' => 'akdlfjs'
        ], CreateEmailRequest::createFromArray([
            'pidm' => 111111,
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isForced' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'comment' => 'kaldf alkdsf lasd',
            'createdBy' => 'akdlfjs'
        ])->toJsonArray());

        $this->assertSame([
            'pidm' => 111111,
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'isPreferred' => false,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'isForced' => false,
            'comment' => 'kaldf alkdsf lasd',
            'createdBy' => 'akdlfjs'
        ], CreateEmailRequest::createFromArray([
            'pidm' => 111111,
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'comment' => 'kaldf alkdsf lasd',
            'createdBy' => 'akdlfjs'
        ])->toJsonArray());
    }
}