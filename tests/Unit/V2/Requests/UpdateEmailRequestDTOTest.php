<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2\Requests;


use Illuminate\Validation\ValidationException;
use MiamiOH\PhpEmailService\Tests\Unit\V2\TestCase;
use MiamiOH\PhpEmailService\V2\Requests\UpdateEmailRequestDTO;

/**
 * @covers \MiamiOH\PhpEmailService\V2\Requests\UpdateEmailRequestDTO
 */
class UpdateEmailRequestDTOTest extends TestCase
{
    public function testFailedToCreateRequestDueToBadRequest()
    {
        $data = [
            [
                'typeCode' => 'ADM',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isActive' => true,
                'isForced' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'askj laksdjf laks df',
                'updatedBy' => 'kjlsdkl',
            ],
            [
                'id' => 'KSLKJDFLKLVS',
                'typeCode' => 'ADM',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isActive' => true,
                'isForced' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'askj laksdjf laks df',
            ],
            [
                'id' => 'KSLKJDFLKLVS',
                'typeCode' => 'ADMSD',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isActive' => true,
                'isForced' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'askj laksdjf laks df',
                'updatedBy' => 'kjlsdkl',
            ],
            [
                'id' => 'KSLKJDFLKLVS',
                'typeCode' => 'ADM',
                'email' => 'aaa@gmail.com',
                'isPreferred' => 'asdf',
                'isActive' => true,
                'isForced' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'askj laksdjf laks df',
                'updatedBy' => 'kjlsdkl',
            ],
            [
                'id' => 'KSLKJDFLKLVS',
                'typeCode' => 'ADM',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isActive' => 'asdf',
                'isForced' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'askj laksdjf laks df',
                'updatedBy' => 'kjlsdkl',
            ],
            [
                'id' => 'KSLKJDFLKLVS',
                'typeCode' => 'ADM',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isActive' => true,
                'isForced' => 'asdf',
                'isDisplayedOnWeb' => true,
                'comment' => 'askj laksdjf laks df',
                'updatedBy' => 'kjlsdkl',
            ],
            [
                'id' => 'KSLKJDFLKLVS',
                'typeCode' => 'ADM',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isActive' => true,
                'isForced' => true,
                'isDisplayedOnWeb' => 'asdf',
                'comment' => 'askj laksdjf laks df',
                'updatedBy' => 'kjlsdkl',
            ],
        ];

        foreach ($data as $d) {
            try {
                UpdateEmailRequestDTO::createFromArray($d);
                $this->fail();
            } catch (ValidationException $e) {
                //
            }
        }

        $this->assertTrue(true);
    }

    public function testCreateRequestFromRequestBody()
    {
        $this->assertSame([
            'id' => 'KSLKJDFLKLVS',
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'isForced' => true,
            'comment' => 'askj laksdjf laks df',
            'isCommentUpdated' => true,
            'updatedBy' => 'kjlsdkl',
        ], UpdateEmailRequestDTO::createFromArray([
            'id' => 'KSLKJDFLKLVS',
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => true,
            'isForced' => true,
            'isDisplayedOnWeb' => true,
            'comment' => 'askj laksdjf laks df',
            'updatedBy' => 'kjlsdkl',
        ])->toJsonArray());

        $this->assertSame([
            'id' => 'KSLKJDFLKLVS',
            'typeCode' => null,
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'isForced' => true,
            'comment' => 'askj laksdjf laks df',
            'isCommentUpdated' => true,
            'updatedBy' => 'kjlsdkl',
        ], UpdateEmailRequestDTO::createFromArray([
            'id' => 'KSLKJDFLKLVS',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => true,
            'isForced' => true,
            'isDisplayedOnWeb' => true,
            'comment' => 'askj laksdjf laks df',
            'updatedBy' => 'kjlsdkl',
        ])->toJsonArray());

        $this->assertSame([
            'id' => 'KSLKJDFLKLVS',
            'typeCode' => 'ADM',
            'email' => null,
            'isPreferred' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'isForced' => true,
            'comment' => 'askj laksdjf laks df',
            'isCommentUpdated' => true,
            'updatedBy' => 'kjlsdkl',
        ], UpdateEmailRequestDTO::createFromArray([
            'id' => 'KSLKJDFLKLVS',
            'typeCode' => 'ADM',
            'isPreferred' => true,
            'isActive' => true,
            'isForced' => true,
            'isDisplayedOnWeb' => true,
            'comment' => 'askj laksdjf laks df',
            'updatedBy' => 'kjlsdkl',
        ])->toJsonArray());

        $this->assertSame([
            'id' => 'KSLKJDFLKLVS',
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'isPreferred' => null,
            'isActive' => null,
            'isDisplayedOnWeb' => null,
            'isForced' => false,
            'comment' => null,
            'isCommentUpdated' => false,
            'updatedBy' => 'kjlsdkl',
        ], UpdateEmailRequestDTO::createFromArray([
            'id' => 'KSLKJDFLKLVS',
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'updatedBy' => 'kjlsdkl',
        ])->toJsonArray());

        $this->assertSame([
            'id' => 'KSLKJDFLKLVS',
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'isForced' => true,
            'comment' => null,
            'isCommentUpdated' => true,
            'updatedBy' => 'kjlsdkl',
        ], UpdateEmailRequestDTO::createFromArray([
            'id' => 'KSLKJDFLKLVS',
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => true,
            'isForced' => true,
            'isDisplayedOnWeb' => true,
            'comment' => null,
            'updatedBy' => 'kjlsdkl',
        ])->toJsonArray());
    }
}