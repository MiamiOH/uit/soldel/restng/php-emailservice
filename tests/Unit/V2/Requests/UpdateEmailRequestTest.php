<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2\Requests;


use Carbon\Carbon;
use MiamiOH\PhpEmailService\Tests\Unit\V2\TestCase;
use MiamiOH\PhpEmailService\V2\Requests\UpdateEmailRequest;

/**
 * @covers \MiamiOH\PhpEmailService\V2\Requests\UpdateEmailRequest
 */
class UpdateEmailRequestTest extends TestCase
{
    public function testCreateRequestFromDTO()
    {
        $this->assertSame([
            'id' => 'AKLDKJSLFSD',
            'pidm' => 1439430,
            'typeCode' => 'MU',
            'email' => 'bb@miamioh.edu',
            'isPreferred' => false,
            'isActive' => false,
            'isDisplayedOnWeb' => false,
            'comment' => 'aaaa',
            'updatedBy' => 'klsdk',
        ], UpdateEmailRequest::create(
            $this->mockEmail([
                'id' => 'AKLDKJSLFSD',
                'pidm' => 1439430,
                'typeCode' => 'ADM',
                'type' => 'Admission Email',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'askl ajsl dja l fasd f',
                'updatedBy' => 'ladbm',
                'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5),
            ]),
            $this->mockUpdateEmailRequestDTO([
                'id' => 'AKLDKJSLFSD',
                'typeCode' => 'MU',
                'email' => 'bb@miamioh.edu',
                'isPreferred' => false,
                'isActive' => false,
                'isDisplayedOnWeb' => false,
                'isForced' => true,
                'comment' => 'aaaa',
                'isCommentUpdated' => true,
                'updatedBy' => 'klsdk',
            ])
        )->toJsonArray());

        $this->assertSame([
            'id' => 'AKLDKJSLFSD',
            'pidm' => 1439430,
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'comment' => 'askl ajsl dja l fasd f',
            'updatedBy' => 'klsdk',
        ], UpdateEmailRequest::create(
            $this->mockEmail([
                'id' => 'AKLDKJSLFSD',
                'pidm' => 1439430,
                'typeCode' => 'ADM',
                'type' => 'Admission Email',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'askl ajsl dja l fasd f',
                'updatedBy' => 'ladbm',
                'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5),
            ]),
            $this->mockUpdateEmailRequestDTO([
                'id' => 'AKLDKJSLFSD',
                'typeCode' => null,
                'email' => null,
                'isPreferred' => null,
                'isActive' => null,
                'isDisplayedOnWeb' => null,
                'comment' => 'aaaa',
                'isCommentUpdated' => false,
                'updatedBy' => 'klsdk',
            ])
        )->toJsonArray());
    }

    public function testCreateRequestOfPreferredEmail()
    {
        $this->assertSame([
            'id' => 'AKLDKJSLFSD',
            'pidm' => 1439430,
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'isPreferred' => false,
            'isActive' => false,
            'isDisplayedOnWeb' => true,
            'comment' => 'askl ajsl dja l fasd f',
            'updatedBy' => 'akjlsd',
        ], UpdateEmailRequest::ofTakingOffPreferredFlag(
            $this->mockEmail([
                'id' => 'AKLDKJSLFSD',
                'pidm' => 1439430,
                'typeCode' => 'ADM',
                'type' => 'Admission Email',
                'email' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isActive' => false,
                'isDisplayedOnWeb' => true,
                'comment' => 'askl ajsl dja l fasd f',
                'updatedBy' => 'ladbm',
                'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5),
            ]),
            'akjlsd'
        )->toJsonArray());
    }
}