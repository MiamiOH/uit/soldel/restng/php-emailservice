<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2\Requests;


use Illuminate\Validation\ValidationException;
use MiamiOH\PhpEmailService\Tests\Unit\V2\TestCase;
use MiamiOH\PhpEmailService\V2\Requests\SearchEmailRequest;

/**
 * @covers \MiamiOH\PhpEmailService\V2\Requests\SearchEmailRequest
 */
class SearchEmailRequestTest extends TestCase
{
    public function testFailedToCreateRequestDueToBadRequest()
    {
        $data = [
            [
                'ids' => null,
                'pidms' => [111111],
                'typeCodes' => ['ADM'],
                'isPreferred' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true
            ],
            [
                'ids' => ['KSDLKJFKLDS'],
                'pidms' => null,
                'typeCodes' => ['ADM'],
                'isPreferred' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true
            ],
            [
                'ids' => ['KSDLKJFKLDS'],
                'pidms' => [111111],
                'typeCodes' => null,
                'isPreferred' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true
            ],
            [
                'ids' => ['KSDLKJFKLDS'],
                'pidms' => ['asdfas'],
                'typeCodes' => ['ADM'],
                'isPreferred' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true
            ],
            [
                'ids' => ['KSDLKJFKLDS'],
                'pidms' => [111111],
                'typeCodes' => ['ADM'],
                'isPreferred' => 'asdf',
                'isActive' => true,
                'isDisplayedOnWeb' => true
            ],
            [
                'ids' => ['KSDLKJFKLDS'],
                'pidms' => [111111],
                'typeCodes' => ['ADM'],
                'isPreferred' => true,
                'isActive' => 'sdfg',
                'isDisplayedOnWeb' => true
            ],
            [
                'ids' => ['KSDLKJFKLDS'],
                'pidms' => [111111],
                'typeCodes' => ['ADM'],
                'isPreferred' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => 'asdfas'
            ],
        ];

        foreach ($data as $d) {
            try {
                SearchEmailRequest::createFromArray($d, 0, 10);
                $this->fail();
            } catch (ValidationException $e) {
                //
            }
        }

        $this->assertTrue(true);
    }

    public function testCreateRequestFromRequestBody()
    {
        $this->assertSame([
            'ids' => ['KSDLKJFKLDS'],
            'pidms' => [111111],
            'typeCodes' => ['ADM'],
            'isPreferred' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'offset' => 0,
            'limit' => 10,
        ], SearchEmailRequest::createFromArray([
            'ids' => ['KSDLKJFKLDS'],
            'pidms' => [111111],
            'typeCodes' => ['ADM'],
            'isPreferred' => 'true',
            'isActive' => 'true',
            'isDisplayedOnWeb' => 'true'
        ], 0, 10)->toJsonArray());

        $this->assertSame([
            'ids' => null,
            'pidms' => null,
            'typeCodes' => null,
            'isPreferred' => null,
            'isActive' => null,
            'isDisplayedOnWeb' => null,
            'offset' => 0,
            'limit' => 10,
        ], SearchEmailRequest::createFromArray([], 0, 10)->toJsonArray());

        $this->assertSame([
            'ids' => ['KSDLKJFKLDS', 'IKLKSDJLFKSDF'],
            'pidms' => [111111, 222222],
            'typeCodes' => ['AD', 'MU'],
            'isPreferred' => false,
            'isActive' => false,
            'isDisplayedOnWeb' => false,
            'offset' => 0,
            'limit' => 10,
        ], SearchEmailRequest::createFromArray([
            'ids' => ['  KSDLKJFKLDS  ', '  IKLKSDJLFKSDF  ', '  KSDLKJFKLDS  '],
            'pidms' => ['  111111  ', '  222222  ', '  111111  '],
            'typeCodes' => [' AD ', ' AD ', ' MU '],
            'isPreferred' => 'false',
            'isActive' => 'false',
            'isDisplayedOnWeb' => 'false'
        ], 0, 10)->toJsonArray());
    }

    public function testCreateRequestOfPreferredEmail()
    {
        $this->assertSame([
            'ids' => null,
            'pidms' => [111111],
            'typeCodes' => null,
            'isPreferred' => true,
            'isActive' => null,
            'isDisplayedOnWeb' => null,
            'offset' => 0,
            'limit' => 1,
        ], SearchEmailRequest::ofPreferredForThePerson(111111)->toJsonArray());
    }
}