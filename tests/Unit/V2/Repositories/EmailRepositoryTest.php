<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2\Repositories;


use Carbon\Carbon;
use Illuminate\Database\Connection;
use MiamiOH\PhpEmailService\Tests\Unit\V2\TestCase;
use MiamiOH\PhpEmailService\V2\Models\EloquentEmail;
use MiamiOH\PhpEmailService\V2\Models\Email;
use MiamiOH\PhpEmailService\V2\Repositories\EmailRepository;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\PhpEmailService\V2\Repositories\EmailRepository
 * @covers \MiamiOH\PhpEmailService\V2\Requests\SearchEmailRequest
 * @covers \MiamiOH\PhpEmailService\V2\Requests\CreateEmailRequest
 * @covers \MiamiOH\PhpEmailService\V2\Requests\UpdateEmailRequest
 */
class EmailRepositoryTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $eloquentEmailModel;
    /**
     * @var MockObject
     */
    private $db;
    /**
     * @var EmailRepository
     */
    private $emailRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->eloquentEmailModel = $this->getMockBuilder(EloquentEmail::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'byId',
                'first',
                'byIds',
                'byPidms',
                'byTypeCodes',
                'byPreferredIndicator',
                'byStatusIndicator',
                'byDisplayWebIndicator',
                'count',
                'orderBy',
                'offset',
                'limit',
                'get'
            ])
            ->getMock();
        $this->db = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'executeProcedure',
                'transaction'
            ])
            ->getMock();
        $this->emailRepository = new EmailRepository(
            $this->eloquentEmailModel,
            $this->db
        );
    }

    public function testGetEmailById()
    {
        $this->eloquentEmailModel->expects($this->once())
            ->method('byId')
            ->with($this->equalTo('ALKDJLFKSD'))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('first')
            ->willReturn($this->mockEloquentEmail([
                'getId' => 'ALKDJLFKSD',
                'getPidm' => 111111,
                'getTypeCode' => 'ADM',
                'getType' => 'Admission Email',
                'getEmail' => 'aaa@gmail.com',
                'isPreferred' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'getComment' => 'jl asdjf lasd jflasd',
                'getUpdatedBy' => 'LAKDLF',
                'getUpdatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5),
            ]));

        $email = $this->emailRepository->get('ALKDJLFKSD');
        $this->assertSame([
            'id' => 'ALKDJLFKSD',
            'pidm' => 111111,
            'typeCode' => 'ADM',
            'type' => 'Admission Email',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'comment' => 'jl asdjf lasd jflasd',
            'updatedBy' => 'lakdlf',
            'updatedAt' => '2020-01-02 03:04:05',
        ], $email->toJsonArray());
    }

    public function testTheEmailWithIDDoesNotExist()
    {
        $this->eloquentEmailModel->expects($this->once())
            ->method('byId')
            ->with($this->equalTo('ALKDJLFKSD'))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('first')
            ->willReturn(null);

        $this->assertNull($this->emailRepository->get('ALKDJLFKSD'));
    }

    public function testSearchEmails()
    {
        $request = $this->mockSearchEmailRequest([
            'ids' => ['LSKEJLKSDFDS', 'LKGJLKJSDLFS'],
            'pidms' => [11111111, 22222222, 33333333],
            'typeCodes' => ['ADM', 'MU'],
            'isPreferred' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'offset' => 3,
            'limit' => 10
        ]);

        $this->eloquentEmailModel->expects($this->once())
            ->method('byIds')
            ->with($this->equalTo(['LSKEJLKSDFDS', 'LKGJLKJSDLFS']))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('byPidms')
            ->with($this->equalTo([11111111, 22222222, 33333333]))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('byTypeCodes')
            ->with($this->equalTo(['ADM', 'MU']))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('byPreferredIndicator')
            ->with($this->equalTo('Y'))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('byStatusIndicator')
            ->with($this->equalTo('A'))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('byDisplayWebIndicator')
            ->with($this->equalTo('Y'))
            ->willReturnSelf();

        $this->eloquentEmailModel->method('count')->willReturn(20);

        $this->eloquentEmailModel->expects($this->once())
            ->method('orderBy')
            ->with($this->equalTo('goremal_surrogate_id'))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('offset')
            ->with($this->equalTo(3))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('limit')
            ->with($this->equalTo(10))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('get')
            ->willReturn($this->mockEloquentEmails(10));

        $emails = $this->emailRepository->search($request);

        $this->assertCount(10, $emails);
        $this->assertSame(20, $emails->getTotalCount());
    }

    public function testSearchEmails2()
    {
        $request = $this->mockSearchEmailRequest([
            'ids' => ['LSKEJLKSDFDS', 'LKGJLKJSDLFS'],
            'pidms' => [11111111, 22222222, 33333333],
            'typeCodes' => ['ADM', 'MU'],
            'isPreferred' => false,
            'isActive' => false,
            'isDisplayedOnWeb' => false,
            'offset' => 3,
            'limit' => 10
        ]);

        $this->eloquentEmailModel->expects($this->once())
            ->method('byIds')
            ->with($this->equalTo(['LSKEJLKSDFDS', 'LKGJLKJSDLFS']))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('byPidms')
            ->with($this->equalTo([11111111, 22222222, 33333333]))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('byTypeCodes')
            ->with($this->equalTo(['ADM', 'MU']))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('byPreferredIndicator')
            ->with($this->equalTo('N'))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('byStatusIndicator')
            ->with($this->equalTo('I'))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('byDisplayWebIndicator')
            ->with($this->equalTo('N'))
            ->willReturnSelf();

        $this->eloquentEmailModel->method('count')->willReturn(20);

        $this->eloquentEmailModel->expects($this->once())
            ->method('orderBy')
            ->with($this->equalTo('goremal_surrogate_id'))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('offset')
            ->with($this->equalTo(3))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('limit')
            ->with($this->equalTo(10))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('get')
            ->willReturn($this->mockEloquentEmails(10));

        $emails = $this->emailRepository->search($request);

        $this->assertCount(10, $emails);
        $this->assertSame(20, $emails->getTotalCount());
    }

    public function testCreateANewEmail()
    {
        $request = $this->mockCreateEmailRequest([
            'pidm' => 111111,
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'isForced' => true,
            'comment' => 'alasjd f asdlf ;ask',
            'createdBy' => 'aldkf',
        ]);

        $this->db->expects($this->once())
            ->method('executeProcedure')
            ->with(
                $this->equalTo('gb_email.p_create'),
                $this->equalTo([
                    'p_pidm' => 111111,
                    'p_emal_code' => 'ADM',
                    'p_email_address' => 'aaa@gmail.com',
                    'p_status_ind' => 'A',
                    'p_preferred_ind' => 'Y',
                    'p_user_id' => 'ALDKF',
                    'p_comment' => 'alasjd f asdlf ;ask',
                    'p_disp_web_ind' => 'Y',
                    'p_data_origin' => EmailRepository::DATA_ORIGIN,
                    'p_rowid_out' => [
                        'value' => null,
                        'length' => 1000
                    ]
                ])
            )
            ->willReturnCallback(function ($func, $bindings) {
                $bindings['p_rowid_out']['value'] = 'KDLSJFKCLS';
                return true;
            });

        $this->eloquentEmailModel->expects($this->once())
            ->method('byId')
            ->with($this->equalTo('KDLSJFKCLS'))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('first')
            ->willReturn($this->mockEloquentEmail());

        $email = $this->emailRepository->create($request);

        $this->assertInstanceOf(Email::class, $email);
    }

    public function testUpdateAnExistingEmail()
    {
        $request = $this->mockUpdateEmailRequest([
            'id' => 'DKLSKJFKL',
            'pidm' => 111111,
            'typeCode' => 'ADM',
            'email' => 'aaa@gmail.com',
            'isPreferred' => false,
            'isActive' => false,
            'isDisplayedOnWeb' => false,
            'comment' => 'alasjd f asdlf ;ask',
            'updatedBy' => 'aldkf',
        ]);

        $this->db->expects($this->once())
            ->method('executeProcedure')
            ->with(
                $this->equalTo('gb_email.p_update'),
                $this->equalTo([
                    'p_pidm' => 111111,
                    'p_emal_code' => 'ADM',
                    'p_email_address' => 'aaa@gmail.com',
                    'p_status_ind' => 'I',
                    'p_preferred_ind' => 'N',
                    'p_user_id' => 'ALDKF',
                    'p_comment' => 'alasjd f asdlf ;ask',
                    'p_disp_web_ind' => 'N',
                    'p_data_origin' => EmailRepository::DATA_ORIGIN,
                    'p_rowid' => 'DKLSKJFKL'
                ])
            )
            ->willReturn(true);

        $this->eloquentEmailModel->expects($this->once())
            ->method('byId')
            ->with($this->equalTo('DKLSKJFKL'))
            ->willReturnSelf();

        $this->eloquentEmailModel->expects($this->once())
            ->method('first')
            ->willReturn($this->mockEloquentEmail());

        $email = $this->emailRepository->update($request);

        $this->assertInstanceOf(Email::class, $email);
    }

    public function testDeleteAnExistingEmail()
    {
        $this->db->expects($this->once())
            ->method('executeProcedure')
            ->with(
                $this->equalTo('gb_email.p_delete'),
                $this->equalTo([
                    'p_pidm' => null,
                    'p_emal_code' => null,
                    'p_email_address' => null,
                    'p_rowid' => 'CKLSKJLKELSKFL',
                ])
            )
            ->willReturn(true);

        $this->emailRepository->delete('CKLSKJLKELSKFL');
        $this->assertTrue(true);
    }

    public function testExecuteAListOfActionsInTransaction()
    {
        $func = function () {
            return true;
        };

        $this->db->expects($this->once())
            ->method('transaction')
            ->with($this->equalTo($func))
            ->willReturn(true);

        $this->assertTrue($this->emailRepository->unitOfWork($func));
    }
}