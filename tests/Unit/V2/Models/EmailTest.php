<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2\Models;


use Carbon\Carbon;
use MiamiOH\PhpEmailService\Tests\Unit\V2\TestCase;

/**
 * @covers \MiamiOH\PhpEmailService\V2\Models\Email
 */
class EmailTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $model = $this->mockEmail([
            'id' => 'AKLDKJSLFSD',
            'pidm' => 1439430,
            'typeCode' => 'ADM',
            'type' => 'Admission Email',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => false,
            'isDisplayedOnWeb' => true,
            'comment' => 'askl ajsl dja l fasd f',
            'updatedBy' => 'ladbm',
            'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5),
        ]);

        $this->assertSame([
            'id' => 'AKLDKJSLFSD',
            'pidm' => 1439430,
            'typeCode' => 'ADM',
            'type' => 'Admission Email',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => false,
            'isDisplayedOnWeb' => true,
            'comment' => 'askl ajsl dja l fasd f',
            'updatedBy' => 'ladbm',
            'updatedAt' => '2020-01-02 03:04:05'
        ], $model->toJsonArray());
    }
}