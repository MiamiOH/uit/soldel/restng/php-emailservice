<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2\Collections;


use MiamiOH\PhpEmailService\Tests\Unit\V2\TestCase;

/**
 * @covers \MiamiOH\PhpEmailService\V2\Collections\EmailCollection
 * @covers \MiamiOH\PhpEmailService\V2\Collections\JsonableCollection
 */
class EmailCollectionTest extends TestCase
{
    public function testConvertCollectionsToArray()
    {
        $emails = $this->mockEmails(10);

        $this->assertCount(10, $emails->toJsonArray());
    }

    public function testSetTotalCountForPageableCollection()
    {
        $emails = $this->mockEmails(10);

        $this->assertCount(10, $emails);
        $this->assertSame(10, $emails->getTotalCount());

        $emails->setTotalCount(20);
        $this->assertSame(20, $emails->getTotalCount());
    }
}