<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2;


use Carbon\Carbon;
use Faker\Generator;
use Illuminate\Database\Eloquent\Collection;
use MiamiOH\PhpEmailService\V2\Collections\EmailCollection;
use MiamiOH\PhpEmailService\V2\Models\EloquentEmail;
use MiamiOH\PhpEmailService\V2\Models\Email;
use MiamiOH\PhpEmailService\V2\Requests\CreateEmailRequest;
use MiamiOH\PhpEmailService\V2\Requests\SearchEmailRequest;
use MiamiOH\PhpEmailService\V2\Requests\UpdateEmailRequest;
use MiamiOH\PhpEmailService\V2\Requests\UpdateEmailRequestDTO;
use PHPUnit\Framework\MockObject\MockObject;

trait MockModels
{
    /**
     * @var Generator
     */
    private $faker;

    /***********************************************************************************
     * Mock Models
     ***********************************************************************************/

    protected function mockEmail(array $data = []): Email
    {
        return $this->mockObject(Email::class, [
            'id' => $this->randomId(),
            'pidm' => $this->randomPidm(),
            'typeCode' => $this->randomEmailTypeCode(),
            'type' => $this->faker->word,
            'email' => $this->faker->email,
            'isPreferred' => $this->faker->boolean,
            'isActive' => $this->faker->boolean,
            'isDisplayedOnWeb' => $this->faker->boolean,
            'comment' => $this->faker->text,
            'updatedBy' => $this->randomUniqueId(),
            'updatedAt' => $this->randomDate(),
        ], $data);
    }

    protected function mockEmails(int $number = 1, array $data = []): EmailCollection
    {
        return $this->mockCollection(
            EmailCollection::class,
            function (array $d) {
                return $this->mockEmail($d);
            },
            $number,
            $data
        );
    }

    protected function mockEloquentEmail(array $data = []): MockObject
    {
        $mockEmail = $this->createMock(EloquentEmail::class);

        $mockEmail->method('getId')->willReturn($this->getValue($data, 'getId', $this->randomId()));
        $mockEmail->method('getPidm')->willReturn($this->getValue($data, 'getPidm', $this->randomPidm()));
        $mockEmail->method('getTypeCode')->willReturn($this->getValue($data, 'getTypeCode', $this->randomEmailTypeCode()));
        $mockEmail->method('getType')->willReturn($this->getValue($data, 'getType', $this->faker->word()));
        $mockEmail->method('getEmail')->willReturn($this->getValue($data, 'getEmail', $this->faker->email));
        $mockEmail->method('isPreferred')->willReturn($this->getValue($data, 'isPreferred', $this->faker->boolean));
        $mockEmail->method('isActive')->willReturn($this->getValue($data, 'isActive', $this->faker->boolean));
        $mockEmail->method('isDisplayedOnWeb')->willReturn($this->getValue($data, 'isDisplayedOnWeb', $this->faker->boolean));
        $mockEmail->method('getComment')->willReturn($this->getValue($data, 'getComment', $this->faker->text));
        $mockEmail->method('getUpdatedBy')->willReturn($this->getValue($data, 'getUpdatedBy', $this->randomUniqueId()));
        $mockEmail->method('getUpdatedAt')->willReturn($this->getValue($data, 'getUpdatedAt', $this->randomDate()));

        return $mockEmail;
    }

    protected function mockEloquentEmails(int $number = 1, array $data = []): Collection
    {
        return $this->mockCollection(
            Collection::class,
            function (array $d) {
                return $this->mockEloquentEmail($d);
            },
            $number,
            $data
        );
    }

    protected function mockCreateEmailRequest(array $data = []): CreateEmailRequest
    {
        return $this->mockObject(CreateEmailRequest::class, [
            'pidm' => $this->randomPidm(),
            'typeCode' => $this->randomEmailTypeCode(),
            'email' => $this->faker->email,
            'isPreferred' => $this->faker->boolean,
            'isActive' => $this->faker->boolean,
            'isDisplayedOnWeb' => $this->faker->boolean,
            'isForced' => $this->faker->boolean,
            'comment' => $this->faker->text,
            'createdBy' => $this->randomUniqueId(),
        ], $data);
    }

    protected function mockSearchEmailRequest(array $data = []): SearchEmailRequest
    {
        return $this->mockObject(SearchEmailRequest::class, [
            'ids' => [$this->randomId()],
            'pidms' => [$this->randomPidm()],
            'typeCodes' => [$this->randomEmailTypeCode()],
            'isPreferred' => $this->faker->boolean,
            'isActive' => $this->faker->boolean,
            'isDisplayedOnWeb' => $this->faker->boolean,
            'offset' => $this->faker->randomNumber(),
            'limit' => $this->faker->randomNumber(),
        ], $data);
    }

    protected function mockUpdateEmailRequest(array $data = []): UpdateEmailRequest
    {
        return $this->mockObject(UpdateEmailRequest::class, [
            'id' => $this->randomId(),
            'pidm' => $this->randomPidm(),
            'typeCode' => $this->randomEmailTypeCode(),
            'email' => $this->faker->email,
            'isPreferred' => $this->faker->boolean,
            'isActive' => $this->faker->boolean,
            'isDisplayedOnWeb' => $this->faker->boolean,
            'comment' => $this->faker->text,
            'updatedBy' => $this->randomUniqueId(),
        ], $data);
    }

    protected function mockUpdateEmailRequestDTO(array $data = []): UpdateEmailRequestDTO
    {
        return $this->mockObject(UpdateEmailRequestDTO::class, [
            'id' => $this->randomId(),
            'typeCode' => $this->randomEmailTypeCode(),
            'email' => $this->faker->email,
            'isPreferred' => $this->faker->boolean,
            'isActive' => $this->faker->boolean,
            'isDisplayedOnWeb' => $this->faker->boolean,
            'isForced' => $this->faker->boolean,
            'comment' => $this->faker->text,
            'isCommentUpdated' => $this->faker->boolean,
            'updatedBy' => $this->randomUniqueId(),
        ], $data);
    }

    /***********************************************************************************
     * Helper functions
     ***********************************************************************************/
    private function mockObject(string $class, array $config = [], array $data = [])
    {
        $c = [];
        foreach ($config as $key => $value) {
            $c[] = $this->getValue($data, $key, $value);
        }

        return new $class(...$c);
    }

    private function mockCollection(string $class, \Closure $mockObject, int $number = 1, array $data = [])
    {
        $collection = new $class();

        for ($i = 0; $i < $number; $i++) {
            $collection->push($mockObject($data[$i] ?? []));
        }

        return $collection;
    }

    private function randomLetterOnlyString(int $length = 10): string
    {
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $this->faker->randomLetter;
        }

        return $str;
    }

    private function randomPidm(): string
    {
        return $this->faker->randomNumber(7);
    }

    private function randomUniqueId(): string
    {
        return $this->randomLetterOnlyString(5) . $this->faker->numberBetween(1, 100);
    }

    private function randomDate(): Carbon
    {
        return Carbon::createFromFormat('Y-m-d', $this->faker->date('Y-m-d'));
    }

    private function getValue(array $data, string $key, $default)
    {
        if (array_key_exists($key, $data)) {
            return $data[$key];
        }

        return $default;
    }

    private function randomId(): string
    {
        return strtoupper($this->randomLetterOnlyString(12));
    }

    private function randomEmailTypeCode(): string
    {
        return $this->faker->randomKey([
            'ADM',
            'MU',
            'WRKE'
        ]);
    }
}