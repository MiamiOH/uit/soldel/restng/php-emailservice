<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2\Services;


use MiamiOH\PhpEmailService\Tests\Unit\V2\TestCase;
use MiamiOH\PhpEmailService\V2\Exceptions\ApplicationException;
use MiamiOH\PhpEmailService\V2\Repositories\EmailRepository;
use MiamiOH\PhpEmailService\V2\Services\GetEmailDomainService;

/**
 * @covers \MiamiOH\PhpEmailService\V2\Services\GetEmailDomainService
 * @covers \MiamiOH\PhpEmailService\V2\Services\BaseDomainService
 */
class GetEmailDomainServiceTest extends TestCase
{
    /**
     * @var EmailRepository
     */
    private $emailRepository;
    /**
     * @var GetEmailDomainService
     */
    private $getEmailDomainService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->emailRepository = $this->createMock(EmailRepository::class);
        $this->getEmailDomainService = new GetEmailDomainService($this->emailRepository);
    }

    public function testGetEmailById()
    {
        $mockEmail = $this->mockEmail();
        $this->emailRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo('KSLKDJFKLS'))
            ->willReturn($mockEmail);
        $this->assertSame($mockEmail, $this->getEmailDomainService->get('KSLKDJFKLS'));
    }

    public function testTheEmailWithIDDoesNotExist()
    {
        $this->emailRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo('KSLKDJFKLS'))
            ->willReturn(null);
        $this->expectException(ApplicationException::class);
        $this->getEmailDomainService->get('KSLKDJFKLS');
    }

    public function testFailedToGetEmailByID()
    {
        $this->emailRepository->expects($this->once())
            ->method('get')
            ->with($this->equalTo('KSLKDJFKLS'))
            ->willThrowException(new \Exception('err'));
        $this->expectException(ApplicationException::class);
        $this->getEmailDomainService->get('KSLKDJFKLS');
    }

    public function testSearchEmails()
    {
        $mockEmails = $this->mockEmails(10);
        $request = $this->mockSearchEmailRequest();

        $this->emailRepository->expects($this->once())
            ->method('search')
            ->with($this->equalTo($request))
            ->willReturn($mockEmails);

        $this->assertSame($mockEmails, $this->getEmailDomainService->search($request));
    }

    public function testFailToSearchEmails()
    {
        $request = $this->mockSearchEmailRequest();

        $this->emailRepository->expects($this->once())
            ->method('search')
            ->with($this->equalTo($request))
            ->willThrowException(new \Exception('err'));

        $this->expectException(ApplicationException::class);
        $this->getEmailDomainService->search($request);
    }
}