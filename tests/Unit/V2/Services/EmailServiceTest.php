<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2\Services;


use MiamiOH\PhpEmailService\Tests\Unit\V2\TestCase;
use MiamiOH\PhpEmailService\V2\Exceptions\ApplicationException;
use MiamiOH\PhpEmailService\V2\Requests\SearchEmailRequest;
use MiamiOH\PhpEmailService\V2\Services\CreateEmailDomainService;
use MiamiOH\PhpEmailService\V2\Services\DeleteEmailDomainService;
use MiamiOH\PhpEmailService\V2\Services\EmailService;
use MiamiOH\PhpEmailService\V2\Services\GetEmailDomainService;
use MiamiOH\PhpEmailService\V2\Services\UpdateEmailDomainService;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\PhpEmailService\V2\Services\EmailService
 */
class EmailServiceTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $getEmailDomainService;
    /**
     * @var MockObject
     */
    private $createEmailDomainService;
    /**
     * @var MockObject
     */
    private $updateEmailDomainService;
    /**
     * @var MockObject
     */
    private $deleteEmailDomainService;
    /**
     * @var MockObject
     */
    private $request;
    /**
     * @var Response
     */
    private $response;
    /**
     * @var MockObject
     */
    private $user;
    /**
     * @var EmailService
     */
    private $emailService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->getEmailDomainService = $this->createMock(GetEmailDomainService::class);
        $this->createEmailDomainService = $this->createMock(CreateEmailDomainService::class);
        $this->updateEmailDomainService = $this->createMock(UpdateEmailDomainService::class);
        $this->deleteEmailDomainService = $this->createMock(DeleteEmailDomainService::class);
        $this->request = $this->createMock(Request::class);
        $this->response = new Response();
        $this->user = $this->createMock(User::class);

        $this->emailService = new EmailService(
            $this->getEmailDomainService,
            $this->createEmailDomainService,
            $this->updateEmailDomainService,
            $this->deleteEmailDomainService
        );
        $this->emailService->setApiUser($this->user);
        $this->emailService->setRequest($this->request);
        $this->emailService->setResponse($this->response);
    }

    public function testGetEmailById()
    {
        $this->request->expects($this->once())
            ->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn('LSKDJFLKJDSKLS');
        $email = $this->mockEmail();
        $this->getEmailDomainService->expects($this->once())
            ->method('get')
            ->with($this->equalTo('LSKDJFLKJDSKLS'))
            ->willReturn($email);
        $res = $this->emailService->get();

        $this->assertSame($email->toJsonArray(), $res->getPayload());
        $this->assertSame(App::API_OK, $res->getStatus());
    }

    public function testTheEmailWithIDDoesNotExist()
    {
        $this->request->expects($this->once())
            ->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn('LSKDJFLKJDSKLS');
        $this->getEmailDomainService->expects($this->once())
            ->method('get')
            ->with($this->equalTo('LSKDJFLKJDSKLS'))
            ->willThrowException(new ApplicationException('asadf'));
        $res = $this->emailService->get();

        $this->assertSame([
            'message' => 'asadf',
            'errors' => []
        ], $res->getPayload());
        $this->assertSame(App::API_NOTFOUND, $res->getStatus());
    }

    public function testDeleteEmailById()
    {
        $this->request->expects($this->once())
            ->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn('LSKDJFLKJDSKLS');
        $this->deleteEmailDomainService->expects($this->once())
            ->method('delete')
            ->with($this->equalTo('LSKDJFLKJDSKLS'))
            ->willReturn(null);
        $res = $this->emailService->delete();

        $this->assertSame([], $res->getPayload());
        $this->assertSame(App::API_OK, $res->getStatus());
    }

    public function testFailToDeleteEmailWhenTheEmailDoesNotExist()
    {
        $this->request->expects($this->once())
            ->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn('LSKDJFLKJDSKLS');
        $this->deleteEmailDomainService->expects($this->once())
            ->method('delete')
            ->with($this->equalTo('LSKDJFLKJDSKLS'))
            ->willThrowException(new ApplicationException('asadf'));
        $res = $this->emailService->delete();
        $this->assertSame(App::API_FAILED, $res->getStatus());
    }

    public function testSearchEmails()
    {
        $this->request->expects($this->once())
            ->method('getOptions')
            ->willReturn([
                'pidms' => [111111, 222222]
            ]);
        $this->request->expects($this->once())
            ->method('getOffset')
            ->willReturn(1);
        $this->request->expects($this->once())
            ->method('getLimit')
            ->willReturn(10);
        $emails = $this->mockEmails(10);
        $emails->setTotalCount(20);
        $this->getEmailDomainService->expects($this->once())
            ->method('search')
            ->with($this->callback(function (SearchEmailRequest $request) {
                $this->assertSame(0, $request->getOffset());
                $this->assertSame(10, $request->getLimit());
                return true;
            }))
            ->willReturn($emails);

        $res = $this->emailService->search();
        $this->assertSame($emails->toJsonArray(), $res->getPayload());
        $this->assertSame(App::API_OK, $res->getStatus());
        $this->assertSame(20, $res->getTotalObjects());
    }

    public function testFailToSearchEmailsWhenTheRequestOptionsAreInvalid()
    {
        $this->request->expects($this->once())
            ->method('getOptions')
            ->willReturn([
                'pidms' => 111111
            ]);
        $this->request->expects($this->once())
            ->method('getOffset')
            ->willReturn(1);
        $this->request->expects($this->once())
            ->method('getLimit')
            ->willReturn(10);

        $res = $this->emailService->search();
        $this->assertSame(App::API_BADREQUEST, $res->getStatus());
    }

    public function testFailToSearchEmailsDueToDbErrors()
    {
        $this->request->expects($this->once())
            ->method('getOptions')
            ->willReturn([
                'pidms' => [111111, 222222]
            ]);
        $this->request->expects($this->once())
            ->method('getOffset')
            ->willReturn(1);
        $this->request->expects($this->once())
            ->method('getLimit')
            ->willReturn(10);
        $emails = $this->mockEmails(10);
        $emails->setTotalCount(20);
        $this->getEmailDomainService->expects($this->once())
            ->method('search')
            ->willThrowException(new ApplicationException('asdf'));

        $res = $this->emailService->search();
        $this->assertSame(App::API_FAILED, $res->getStatus());
    }

    public function testCreateANewEmail()
    {
        $this->user->expects($this->once())
            ->method('getUsername')
            ->willReturn('alkdfj');
        $this->request->expects($this->once())
            ->method('getData')
            ->willReturn([
                'pidm' => 1829374,
                'typeCode' => 'ADM',
                'email' => 'john.doe@gmail.com',
                'isPreferred' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'Registrar collects the email from the student.',
                'createdBy' => 'ladmn'
            ]);
        $email = $this->mockEmail();
        $this->createEmailDomainService->expects($this->once())
            ->method('create')
            ->willReturn($email);
        $res = $this->emailService->create();
        $this->assertSame($email->toJsonArray(), $res->getPayload());
        $this->assertSame(App::API_OK, $res->getStatus());
    }

    public function testFailedToCreateANewEmailWhenTheRequestBodyIsInvalid()
    {
        $this->user->expects($this->once())
            ->method('getUsername')
            ->willReturn('alkdfj');
        $this->request->expects($this->once())
            ->method('getData')
            ->willReturn([
                'typeCode' => 'ADM',
                'email' => 'john.doe@gmail.com',
                'isPreferred' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'Registrar collects the email from the student.',
                'createdBy' => 'ladmn'
            ]);

        $res = $this->emailService->create();
        $this->assertSame(App::API_BADREQUEST, $res->getStatus());
    }

    public function testFailedToCreateANewEmailWhenThereIsADBError()
    {
        $this->user->expects($this->once())
            ->method('getUsername')
            ->willReturn('alkdfj');
        $this->request->expects($this->once())
            ->method('getData')
            ->willReturn([
                'pidm' => 1829374,
                'typeCode' => 'ADM',
                'email' => 'john.doe@gmail.com',
                'isPreferred' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'Registrar collects the email from the student.',
                'createdBy' => 'ladmn'
            ]);
        $this->createEmailDomainService->expects($this->once())
            ->method('create')
            ->willThrowException(new ApplicationException('asdf'));
        $res = $this->emailService->create();
        $this->assertSame(App::API_FAILED, $res->getStatus());
    }

    public function testUpdateAnExistingEmail()
    {
        $this->request->expects($this->once())
            ->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn('AAAWVzABNAAGiRYAAZ');
        $this->user->expects($this->once())
            ->method('getUsername')
            ->willReturn('alkdfj');
        $this->request->expects($this->once())
            ->method('getData')
            ->willReturn([
                'typeCode' => 'ADM',
                'email' => 'john.doe@gmail.com',
                'isPreferred' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'Registrar collects the email from the student.',
                'updatedBy' => 'lanbdm'
            ]);
        $email = $this->mockEmail();
        $this->updateEmailDomainService->expects($this->once())
            ->method('update')
            ->willReturn($email);
        $res = $this->emailService->update();
        $this->assertSame($email->toJsonArray(), $res->getPayload());
        $this->assertSame(App::API_OK, $res->getStatus());
    }

    public function testFailedToUpdateAnExistingEmailWhenTheRequestBodyIsInvalid()
    {
        $this->request->expects($this->once())
            ->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn('AAAWVzABNAAGiRYAAZ');
        $this->user->expects($this->once())
            ->method('getUsername')
            ->willReturn('alkdfj');
        $this->request->expects($this->once())
            ->method('getData')
            ->willReturn([]);

        $res = $this->emailService->update();
        $this->assertSame(App::API_BADREQUEST, $res->getStatus());
    }

    public function testFailedToUpdateAnExistingEmailWhenThereIsADBError()
    {
        $this->request->expects($this->once())
            ->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn('AAAWVzABNAAGiRYAAZ');
        $this->user->expects($this->once())
            ->method('getUsername')
            ->willReturn('alkdfj');
        $this->request->expects($this->once())
            ->method('getData')
            ->willReturn([
                'typeCode' => 'ADM',
                'email' => 'john.doe@gmail.com',
                'isPreferred' => true,
                'isActive' => true,
                'isDisplayedOnWeb' => true,
                'comment' => 'Registrar collects the email from the student.',
                'updatedBy' => 'lanbdm'
            ]);
        $this->updateEmailDomainService->expects($this->once())
            ->method('update')
            ->willThrowException(new ApplicationException('asdf'));
        $res = $this->emailService->update();
        $this->assertSame(App::API_FAILED, $res->getStatus());
    }
}