<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2\Services;


use MiamiOH\PhpEmailService\Tests\Unit\V2\TestCase;
use MiamiOH\PhpEmailService\V2\Exceptions\ApplicationException;
use MiamiOH\PhpEmailService\V2\Repositories\EmailRepository;
use MiamiOH\PhpEmailService\V2\Requests\SearchEmailRequest;
use MiamiOH\PhpEmailService\V2\Services\CreateEmailDomainService;
use MiamiOH\PhpEmailService\V2\Services\GetEmailDomainService;

/**
 * @covers \MiamiOH\PhpEmailService\V2\Services\CreateEmailDomainService
 * @covers \MiamiOH\PhpEmailService\V2\Services\BaseDomainService
 */
class CreateEmailDomainServiceTest extends TestCase
{
    /**
     * @var EmailRepository
     */
    private $emailRepository;
    /**
     * @var GetEmailDomainService
     */
    private $getEmailDomainService;
    /**
     * @var CreateEmailDomainService
     */
    private $createEmailDomainService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->emailRepository = $this->createMock(EmailRepository::class);
        $this->getEmailDomainService = $this->createMock(GetEmailDomainService::class);
        $this->createEmailDomainService = new CreateEmailDomainService(
            $this->emailRepository,
            $this->getEmailDomainService
        );
    }

    public function testCreateTheNewEmailDirectlyIfItIsNotPreferred()
    {
        $mockEmail = $this->mockEmail();
        $request = $this->mockCreateEmailRequest([
            'isPreferred' => false
        ]);
        $this->emailRepository
            ->expects($this->once())
            ->method('create')
            ->with($this->equalTo($request))
            ->willReturn($mockEmail);
        $email = $this->createEmailDomainService->create($request);
        $this->assertSame($mockEmail, $email);
    }

    public function testCreateANonPreferredEmail()
    {
        $mockEmail = $this->mockEmail();
        $request = $this->mockCreateEmailRequest([
            'isPreferred' => false,
            'isForced' => true
        ]);
        $this->emailRepository
            ->expects($this->once())
            ->method('create')
            ->with($this->equalTo($request))
            ->willReturn($mockEmail);
        $email = $this->createEmailDomainService->create($request);
        $this->assertSame($mockEmail, $email);
    }

    public function testCreateTheNewEmailDirectlyIfItIsPreferredButNotForce()
    {
        $mockEmail = $this->mockEmail();
        $request = $this->mockCreateEmailRequest([
            'isPreferred' => true,
            'isForced' => false
        ]);
        $this->emailRepository
            ->expects($this->once())
            ->method('create')
            ->with($this->equalTo($request))
            ->willReturn($mockEmail);
        $email = $this->createEmailDomainService->create($request);
        $this->assertSame($mockEmail, $email);
    }

    public function testCreateAPreferredEmailWhileThePersonDoesNotHaveAPreferredEmailYet()
    {
        $mockEmail = $this->mockEmail();
        $request = $this->mockCreateEmailRequest([
            'isPreferred' => true,
            'isForced' => true,
            'pidm' => 111111
        ]);
        $this->getEmailDomainService->expects($this->once())
            ->method('search')
            ->with($this->callback(function ($req) {
                $this->assertInstanceOf(SearchEmailRequest::class, $req);
                $this->assertSame([
                    'ids' => null,
                    'pidms' => [111111],
                    'typeCodes' => null,
                    'isPreferred' => true,
                    'isActive' => null,
                    'isDisplayedOnWeb' => null,
                    'offset' => 0,
                    'limit' => 1,
                ], $req->toJsonArray());
                return true;
            }))
            ->willReturn($this->mockEmails(0));
        $this->emailRepository
            ->expects($this->once())
            ->method('create')
            ->with($this->equalTo($request))
            ->willReturn($mockEmail);
        $email = $this->createEmailDomainService->create($request);
        $this->assertSame($mockEmail, $email);
    }

    public function testCreateAPreferredEmailWhileThePersonAlreadyHasAPreferredEmail()
    {
        $mockEmail = $this->mockEmail();
        $request = $this->mockCreateEmailRequest([
            'isPreferred' => true,
            'isForced' => true,
            'pidm' => 111111
        ]);
        $preferredEmails = $this->mockEmails(1, [
            [
                'isPreferred' => true
            ]
        ]);
        $this->getEmailDomainService->expects($this->once())
            ->method('search')
            ->with($this->callback(function ($req) {
                $this->assertInstanceOf(SearchEmailRequest::class, $req);
                $this->assertSame([
                    'ids' => null,
                    'pidms' => [111111],
                    'typeCodes' => null,
                    'isPreferred' => true,
                    'isActive' => null,
                    'isDisplayedOnWeb' => null,
                    'offset' => 0,
                    'limit' => 1,
                ], $req->toJsonArray());
                return true;
            }))
            ->willReturn($preferredEmails);

        $this->emailRepository->expects($this->once())
            ->method('unitOfWOrk')
            ->willReturn($mockEmail);
        $email = $this->createEmailDomainService->create($request);
        $this->assertSame($mockEmail, $email);
    }

    public function testFailedToCreateANewEmail()
    {
        $request = $this->mockCreateEmailRequest([
            'isPreferred' => false
        ]);
        $this->emailRepository
            ->expects($this->once())
            ->method('create')
            ->with($this->equalTo($request))
            ->willThrowException(new \Exception("
            Error Message : ORA-10394: ::this is an error:: dsfgasdf
            "));

        $this->expectException(ApplicationException::class);
        $this->expectExceptionMessage('this is an error');
        $this->createEmailDomainService->create($request);
    }
}