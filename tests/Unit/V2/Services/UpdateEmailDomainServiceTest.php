<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2\Services;


use Carbon\Carbon;
use MiamiOH\PhpEmailService\Tests\Unit\V2\TestCase;
use MiamiOH\PhpEmailService\V2\Exceptions\ApplicationException;
use MiamiOH\PhpEmailService\V2\Repositories\EmailRepository;
use MiamiOH\PhpEmailService\V2\Requests\SearchEmailRequest;
use MiamiOH\PhpEmailService\V2\Requests\UpdateEmailRequest;
use MiamiOH\PhpEmailService\V2\Services\GetEmailDomainService;
use MiamiOH\PhpEmailService\V2\Services\UpdateEmailDomainService;

/**
 * @covers \MiamiOH\PhpEmailService\V2\Services\UpdateEmailDomainService
 * @covers \MiamiOH\PhpEmailService\V2\Services\BaseDomainService
 */
class UpdateEmailDomainServiceTest extends TestCase
{
    /**
     * @var EmailRepository
     */
    private $emailRepository;
    /**
     * @var GetEmailDomainService
     */
    private $getEmailDomainService;
    /**
     * @var UpdateEmailDomainService
     */
    private $updateEmailDomainService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->emailRepository = $this->createMock(EmailRepository::class);
        $this->getEmailDomainService = $this->createMock(GetEmailDomainService::class);
        $this->updateEmailDomainService = new UpdateEmailDomainService(
            $this->emailRepository,
            $this->getEmailDomainService
        );
    }

    public function testUpdateEmailToNonPreferred()
    {
        $request = $this->mockUpdateEmailRequestDTO([
            'id' => 'AKLDKJSLFSD',
            'typeCode' => 'MU',
            'email' => 'bbb@gmail.com',
            'isPreferred' => false,
            'isActive' => false,
            'isDisplayedOnWeb' => false,
            'isForced' => false,
            'comment' => 'jlsdjafka asdlfj',
            'isCommentUpdated' => true,
            'updatedBy' => 'lakdjf',
        ]);

        $email = $this->mockEmail([
            'id' => 'AKLDKJSLFSD',
            'pidm' => 1439430,
            'typeCode' => 'ADM',
            'type' => 'Admission Email',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'comment' => 'askl ajsl dja l fasd f',
            'updatedBy' => 'ladbm',
            'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5),
        ]);

        $this->getEmailDomainService->expects($this->once())
            ->method('get')
            ->with($this->equalTo('AKLDKJSLFSD'))
            ->willReturn($email);

        $updatedEmail = $this->mockEmail();

        $this->emailRepository->expects($this->once())
            ->method('update')
            ->with($this->callback(function (UpdateEmailRequest $req) {
                $this->assertSame([
                    'id' => 'AKLDKJSLFSD',
                    'pidm' => 1439430,
                    'typeCode' => 'MU',
                    'email' => 'bbb@gmail.com',
                    'isPreferred' => false,
                    'isActive' => false,
                    'isDisplayedOnWeb' => false,
                    'comment' => 'jlsdjafka asdlfj',
                    'updatedBy' => 'lakdjf',
                ], $req->toJsonArray());
                return true;
            }))
            ->willReturn($updatedEmail);

        $this->assertSame($updatedEmail, $this->updateEmailDomainService->update($request));
    }

    public function testUpdateEmailToSetAsPreferredWithNoForce()
    {
        $request = $this->mockUpdateEmailRequestDTO([
            'id' => 'AKLDKJSLFSD',
            'typeCode' => 'MU',
            'email' => 'bbb@gmail.com',
            'isPreferred' => true,
            'isActive' => false,
            'isDisplayedOnWeb' => false,
            'isForced' => false,
            'comment' => 'jlsdjafka asdlfj',
            'isCommentUpdated' => true,
            'updatedBy' => 'lakdjf',
        ]);

        $email = $this->mockEmail([
            'id' => 'AKLDKJSLFSD',
            'pidm' => 1439430,
            'typeCode' => 'ADM',
            'type' => 'Admission Email',
            'email' => 'aaa@gmail.com',
            'isPreferred' => false,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'comment' => 'askl ajsl dja l fasd f',
            'updatedBy' => 'ladbm',
            'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5),
        ]);

        $this->getEmailDomainService->expects($this->once())
            ->method('get')
            ->with($this->equalTo('AKLDKJSLFSD'))
            ->willReturn($email);

        $updatedEmail = $this->mockEmail();

        $this->emailRepository->expects($this->once())
            ->method('update')
            ->with($this->callback(function (UpdateEmailRequest $req) {
                $this->assertSame([
                    'id' => 'AKLDKJSLFSD',
                    'pidm' => 1439430,
                    'typeCode' => 'MU',
                    'email' => 'bbb@gmail.com',
                    'isPreferred' => true,
                    'isActive' => false,
                    'isDisplayedOnWeb' => false,
                    'comment' => 'jlsdjafka asdlfj',
                    'updatedBy' => 'lakdjf',
                ], $req->toJsonArray());
                return true;
            }))
            ->willReturn($updatedEmail);

        $this->assertSame($updatedEmail, $this->updateEmailDomainService->update($request));
    }

    public function testUpdateEmailWhenTheNewPreferredFlagIsSameAsCurrentPreferredFlag()
    {
        $request = $this->mockUpdateEmailRequestDTO([
            'id' => 'AKLDKJSLFSD',
            'typeCode' => 'MU',
            'email' => 'bbb@gmail.com',
            'isPreferred' => true,
            'isActive' => false,
            'isDisplayedOnWeb' => false,
            'isForced' => false,
            'comment' => 'jlsdjafka asdlfj',
            'isCommentUpdated' => true,
            'updatedBy' => 'lakdjf',
        ]);

        $email = $this->mockEmail([
            'id' => 'AKLDKJSLFSD',
            'pidm' => 1439430,
            'typeCode' => 'ADM',
            'type' => 'Admission Email',
            'email' => 'aaa@gmail.com',
            'isPreferred' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'comment' => 'askl ajsl dja l fasd f',
            'updatedBy' => 'ladbm',
            'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5),
        ]);

        $this->getEmailDomainService->expects($this->once())
            ->method('get')
            ->with($this->equalTo('AKLDKJSLFSD'))
            ->willReturn($email);

        $updatedEmail = $this->mockEmail();

        $this->emailRepository->expects($this->once())
            ->method('update')
            ->with($this->callback(function (UpdateEmailRequest $req) {
                $this->assertSame([
                    'id' => 'AKLDKJSLFSD',
                    'pidm' => 1439430,
                    'typeCode' => 'MU',
                    'email' => 'bbb@gmail.com',
                    'isPreferred' => true,
                    'isActive' => false,
                    'isDisplayedOnWeb' => false,
                    'comment' => 'jlsdjafka asdlfj',
                    'updatedBy' => 'lakdjf',
                ], $req->toJsonArray());
                return true;
            }))
            ->willReturn($updatedEmail);

        $this->assertSame($updatedEmail, $this->updateEmailDomainService->update($request));
    }

    public function testUpdateEmailToSetAsPreferredWhenThereIsNoPreferredEmail()
    {
        $request = $this->mockUpdateEmailRequestDTO([
            'id' => 'AKLDKJSLFSD',
            'typeCode' => 'MU',
            'email' => 'bbb@gmail.com',
            'isPreferred' => true,
            'isActive' => false,
            'isDisplayedOnWeb' => false,
            'isForced' => true,
            'comment' => 'jlsdjafka asdlfj',
            'isCommentUpdated' => true,
            'updatedBy' => 'lakdjf',
        ]);

        $email = $this->mockEmail([
            'id' => 'AKLDKJSLFSD',
            'pidm' => 1439430,
            'typeCode' => 'ADM',
            'type' => 'Admission Email',
            'email' => 'aaa@gmail.com',
            'isPreferred' => false,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'comment' => 'askl ajsl dja l fasd f',
            'updatedBy' => 'ladbm',
            'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5),
        ]);

        $this->getEmailDomainService->expects($this->once())
            ->method('get')
            ->with($this->equalTo('AKLDKJSLFSD'))
            ->willReturn($email);

        $updatedEmail = $this->mockEmail();

        $this->emailRepository->expects($this->once())
            ->method('update')
            ->with($this->callback(function (UpdateEmailRequest $req) {
                $this->assertSame([
                    'id' => 'AKLDKJSLFSD',
                    'pidm' => 1439430,
                    'typeCode' => 'MU',
                    'email' => 'bbb@gmail.com',
                    'isPreferred' => true,
                    'isActive' => false,
                    'isDisplayedOnWeb' => false,
                    'comment' => 'jlsdjafka asdlfj',
                    'updatedBy' => 'lakdjf',
                ], $req->toJsonArray());
                return true;
            }))
            ->willReturn($updatedEmail);

        $this->getEmailDomainService->expects($this->once())
            ->method('search')
            ->with($this->callback(function (SearchEmailRequest $request) {
                $this->assertSame([
                    'ids' => null,
                    'pidms' => [1439430],
                    'typeCodes' => null,
                    'isPreferred' => true,
                    'isActive' => null,
                    'isDisplayedOnWeb' => null,
                    'offset' => 0,
                    'limit' => 1,
                ], $request->toJsonArray());
                return true;
            }))
            ->willReturn($this->mockEmails(0));

        $this->assertSame($updatedEmail, $this->updateEmailDomainService->update($request));
    }

    public function testUpdateEmailToSetAsPreferredWhenThereIsAlreadyAPreferredEmail()
    {
        $request = $this->mockUpdateEmailRequestDTO([
            'id' => 'AKLDKJSLFSD',
            'typeCode' => 'MU',
            'email' => 'bbb@gmail.com',
            'isPreferred' => true,
            'isActive' => false,
            'isDisplayedOnWeb' => false,
            'isForced' => true,
            'comment' => 'jlsdjafka asdlfj',
            'isCommentUpdated' => true,
            'updatedBy' => 'lakdjf',
        ]);

        $email = $this->mockEmail([
            'id' => 'AKLDKJSLFSD',
            'pidm' => 1439430,
            'typeCode' => 'ADM',
            'type' => 'Admission Email',
            'email' => 'aaa@gmail.com',
            'isPreferred' => false,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'comment' => 'askl ajsl dja l fasd f',
            'updatedBy' => 'ladbm',
            'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5),
        ]);

        $this->getEmailDomainService->expects($this->once())
            ->method('get')
            ->with($this->equalTo('AKLDKJSLFSD'))
            ->willReturn($email);

        $updatedEmail = $this->mockEmail();

        $this->emailRepository->expects($this->once())
            ->method('unitOfWork')
            ->willReturn($updatedEmail);

        $this->getEmailDomainService->expects($this->once())
            ->method('search')
            ->with($this->callback(function (SearchEmailRequest $request) {
                $this->assertSame([
                    'ids' => null,
                    'pidms' => [1439430],
                    'typeCodes' => null,
                    'isPreferred' => true,
                    'isActive' => null,
                    'isDisplayedOnWeb' => null,
                    'offset' => 0,
                    'limit' => 1,
                ], $request->toJsonArray());
                return true;
            }))
            ->willReturn($this->mockEmails(1));

        $this->assertSame($updatedEmail, $this->updateEmailDomainService->update($request));
    }

    public function testFailedToUpdateAnExistingEmail()
    {
        $request = $this->mockUpdateEmailRequestDTO([
            'isPreferred' => false
        ]);
        $this->emailRepository
            ->expects($this->once())
            ->method('update')
            ->willThrowException(new \Exception("
            Error Message : ORA-10394: ::this is an error:: dsfgasdf
            "));

        $this->expectException(ApplicationException::class);
        $this->expectExceptionMessage('this is an error');
        $this->updateEmailDomainService->update($request);
    }
}