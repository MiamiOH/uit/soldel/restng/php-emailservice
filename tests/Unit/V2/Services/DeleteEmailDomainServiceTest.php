<?php


namespace MiamiOH\PhpEmailService\Tests\Unit\V2\Services;


use MiamiOH\PhpEmailService\Tests\Unit\V2\TestCase;
use MiamiOH\PhpEmailService\V2\Exceptions\ApplicationException;
use MiamiOH\PhpEmailService\V2\Repositories\EmailRepository;
use MiamiOH\PhpEmailService\V2\Services\DeleteEmailDomainService;
use Yajra\Pdo\Oci8\Exceptions\Oci8Exception;

/**
 * @covers \MiamiOH\PhpEmailService\V2\Services\DeleteEmailDomainService
 * @covers \MiamiOH\PhpEmailService\V2\Services\BaseDomainService
 */
class DeleteEmailDomainServiceTest extends TestCase
{
    /**
     * @var EmailRepository
     */
    private $emailRepository;
    /**
     * @var DeleteEmailDomainService
     */
    private $deleteEmailDomainService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->emailRepository = $this->createMock(EmailRepository::class);
        $this->deleteEmailDomainService = new DeleteEmailDomainService($this->emailRepository);
    }

    public function testDeleteAnExistingEmail()
    {
        $this->emailRepository->expects($this->once())
            ->method('delete')
            ->with($this->equalTo('KSLDKFJLSKDL'))
            ->willReturn(null);

        $this->deleteEmailDomainService->delete('KSLDKFJLSKDL');
        $this->assertTrue(true);
    }

    public function testFailToDeleteAnExistingEmail()
    {
        $this->emailRepository->expects($this->once())
            ->method('delete')
            ->with($this->equalTo('KSLDKFJLSKDL'))
            ->willThrowException(new Oci8Exception("
            Error Message : ORA-10394: this is an error
            "));

        $this->expectException(ApplicationException::class);
        $this->expectExceptionMessage('this is an error');
        $this->deleteEmailDomainService->delete('KSLDKFJLSKDL');
    }
}