<?php

/*
-----------------------------------------------------------
FILE NAME: getEmailTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Erin Mills

DESCRIPTION: 
This php class is used to test the GET method of the email service. 

ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit
Person/Email Service

TABLE USAGE:

Web Service Usage:
	Person/Email service (GET)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/14/2015               MILLSE
Description:  Initial Draft
			 
-----------------------------------------------------------
 */

namespace MiamiOH\PhpEmailService\Services;

class GetEmailTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $email, $user, $dbh;

    /**
     * This setUp method is run before every test method.
     * It sets up a mock API, DBH and DB object with methods that the email class uses
     * and injects those into the Email object.
     */
    protected function setUp()
    {

        //Create the mock API object and define its newResponse method
        $api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            //->setMethods(array('newResponse', 'callResource', 'locationFor'))
            ->setMethods(array('newResponse'))
            ->getMock();
        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        //set up the mock database object and its getHandle method
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();
        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock user
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        //Create the email object we will be unit testing and pass it the mock objects
        $this->email = new \MiamiOH\PhpEmailService\Services\Email();
        $this->email->setDatabase($db);
        $this->email->setApiUser($this->user);

    }

    public function testGetEmailSinglePidm()
    {

        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('pidm' => array($this->mockDbRecordsSinglePidm[0]['goremal_pidm']), 'token' => 'blahblah'));
        $this->email->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        //define the querry_all method for this test
        $this->dbh->method('queryall_array')
            ->willReturn($this->mockDbRecordsSinglePidm);

        /*** Call the getEmail and get the payload ***/
        $resp = $this->email->getEmail();
        $payload = $resp->getPayload();

        /*** Make assertions ***/
        //check status and general response
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus()); //we expect the status to be API_OK
        $this->assertTrue(is_array($payload)); //we expect the payload to be an array

        //check payload
        $this->assertEquals($this->mockDbRecordsSinglePidm[0]['goremal_pidm'], $payload['emailRecord0']['pidm']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[0]['goremal_emal_code'], $payload['emailRecord0']['emailCode']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[0]['goremal_email_address'], $payload['emailRecord0']['emailAddress']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[0]['goremal_status_ind'], $payload['emailRecord0']['statusInd']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[0]['goremal_preferred_ind'], $payload['emailRecord0']['preferredInd']);
        //$this->assertEquals($this->mockDbRecordsSinglePidm[0]['goremal_activity_date'], $payload['emailRecord0']['activity_date']);
        //$this->assertEquals($this->mockDbRecordsSinglePidm[0]['goremal_user_id'], $payload['emailRecord0']['user_id']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[0]['goremal_comment'], $payload['emailRecord0']['comment']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[0]['goremal_disp_web_ind'], $payload['emailRecord0']['dispWebInd']);

        $this->assertEquals($this->mockDbRecordsSinglePidm[1]['goremal_pidm'], $payload['emailRecord1']['pidm']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[1]['goremal_emal_code'], $payload['emailRecord1']['emailCode']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[1]['goremal_email_address'], $payload['emailRecord1']['emailAddress']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[1]['goremal_status_ind'], $payload['emailRecord1']['statusInd']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[1]['goremal_preferred_ind'], $payload['emailRecord1']['preferredInd']);
        //$this->assertEquals($this->mockDbRecordsSinglePidm[1]['goremal_activity_date'], $payload['emailRecord1']['activity_date']);
        //$this->assertEquals($this->mockDbRecordsSinglePidm[1]['goremal_user_id'], $payload['emailRecord1']['user_id']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[1]['goremal_comment'], $payload['emailRecord1']['comment']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[1]['goremal_disp_web_ind'], $payload['emailRecord1']['dispWebInd']);

        $this->assertEquals($this->mockDbRecordsSinglePidm[2]['goremal_pidm'], $payload['emailRecord2']['pidm']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[2]['goremal_emal_code'], $payload['emailRecord2']['emailCode']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[2]['goremal_email_address'], $payload['emailRecord2']['emailAddress']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[2]['goremal_status_ind'], $payload['emailRecord2']['statusInd']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[2]['goremal_preferred_ind'], $payload['emailRecord2']['preferredInd']);
        //$this->assertEquals($this->mockDbRecordsSinglePidm[2]['goremal_activity_date'], $payload['emailRecord2']['activity_date']);
        //$this->assertEquals($this->mockDbRecordsSinglePidm[2]['goremal_user_id'], $payload['emailRecord2']['user_id']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[2]['goremal_comment'], $payload['emailRecord2']['comment']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[2]['goremal_disp_web_ind'], $payload['emailRecord2']['dispWebInd']);
    }

    public function testGetEmailMultiPidm()
    {

        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('pidm' => array($this->mockDbRecordsMultiPidm[0]['goremal_pidm'], $this->mockDbRecordsMultiPidm[3]['goremal_pidm']), 'token' => 'blahblah'));
        $this->email->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        //define the querry_all method for this test
        $this->dbh->method('queryall_array')
            ->willReturn($this->mockDbRecordsMultiPidm);

        /*** Call the getEmail and get the payload ***/
        $resp = $this->email->getEmail();
        $payload = $resp->getPayload();

        /*** Make assertions ***/
        //check status and general response
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus()); //we expect the status to be API_OK
        $this->assertTrue(is_array($payload)); //we expect the payload to be an array

        //check payload
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['goremal_pidm'], $payload['emailRecord0']['pidm']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['goremal_emal_code'], $payload['emailRecord0']['emailCode']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['goremal_email_address'], $payload['emailRecord0']['emailAddress']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['goremal_status_ind'], $payload['emailRecord0']['statusInd']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['goremal_preferred_ind'], $payload['emailRecord0']['preferredInd']);
        //$this->assertEquals($this->mockDbRecordsMultiPidm[0]['goremal_activity_date'], $payload['emailRecord0']['activity_date']);
        //$this->assertEquals($this->mockDbRecordsMultiPidm[0]['goremal_user_id'], $payload['emailRecord0']['user_id']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['goremal_comment'], $payload['emailRecord0']['comment']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['goremal_disp_web_ind'], $payload['emailRecord0']['dispWebInd']);

        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['goremal_pidm'], $payload['emailRecord1']['pidm']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['goremal_emal_code'], $payload['emailRecord1']['emailCode']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['goremal_email_address'], $payload['emailRecord1']['emailAddress']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['goremal_status_ind'], $payload['emailRecord1']['statusInd']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['goremal_preferred_ind'], $payload['emailRecord1']['preferredInd']);
        //$this->assertEquals($this->mockDbRecordsMultiPidm[1]['goremal_activity_date'], $payload['emailRecord1']['activity_date']);
        //$this->assertEquals($this->mockDbRecordsMultiPidm[1]['goremal_user_id'], $payload['emailRecord1']['user_id']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['goremal_comment'], $payload['emailRecord1']['comment']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['goremal_disp_web_ind'], $payload['emailRecord1']['dispWebInd']);

        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['goremal_pidm'], $payload['emailRecord2']['pidm']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['goremal_emal_code'], $payload['emailRecord2']['emailCode']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['goremal_email_address'], $payload['emailRecord2']['emailAddress']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['goremal_status_ind'], $payload['emailRecord2']['statusInd']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['goremal_preferred_ind'], $payload['emailRecord2']['preferredInd']);
        //$this->assertEquals($this->mockDbRecordsMultiPidm[2]['goremal_activity_date'], $payload['emailRecord2']['activity_date']);
        //$this->assertEquals($this->mockDbRecordsMultiPidm[2]['goremal_user_id'], $payload['emailRecord2']['user_id']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['goremal_comment'], $payload['emailRecord2']['comment']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['goremal_disp_web_ind'], $payload['emailRecord2']['dispWebInd']);

        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['goremal_pidm'], $payload['emailRecord3']['pidm']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['goremal_emal_code'], $payload['emailRecord3']['emailCode']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['goremal_email_address'], $payload['emailRecord3']['emailAddress']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['goremal_status_ind'], $payload['emailRecord3']['statusInd']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['goremal_preferred_ind'], $payload['emailRecord3']['preferredInd']);
        //$this->assertEquals($this->mockDbRecordsMultiPidm[3]['goremal_activity_date'], $payload['emailRecord3']['activity_date']);
        //$this->assertEquals($this->mockDbRecordsMultiPidm[3]['goremal_user_id'], $payload['emailRecord3']['user_id']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['goremal_comment'], $payload['emailRecord3']['comment']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['goremal_disp_web_ind'], $payload['emailRecord3']['dispWebInd']);
    }

    public function testGetEmailNoRecords()
    {
        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('pidm' => array($this->mockDbRecordsSinglePidm[0]['goremal_pidm']), 'token' => 'blahblah'));
        $this->email->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        //define the querry_all method for this test
        $this->dbh->method('queryall_array')
            ->willReturn(array());

        /*** Call the getEmail and get the payload ***/
        $resp = $this->email->getEmail();
        $payload = $resp->getPayload();

        /*** Make assertions ***/
        //check status and general response
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus()); //we expect the status to be API_OK
        $this->assertTrue(is_array($payload)); //we expect the payload to be an array
        $this->assertEquals(count($payload), 0); //expect that even though the payload is an array that there is nothing in it.
    }

    public function testGetEmailNoOptionsWithToken()
    {
        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('token' => 'blahblah'));
        $this->email->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        //define the querry_all method for this test
        $this->dbh->method('queryall_array')
            ->willReturn(array());

        /*** Call the getEmail ***/
        try {
            $resp = $this->email->getEmail();
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "Error getting options or parameter: No pidms or uniqueids are available.");
        }
    }

    public function testGetEmailNoOptions()
    {
        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array());
        $this->email->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'notAuthorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        //define the querry_all method for this test
        $this->dbh->method('queryall_array')
            ->willReturn(array());

        /*** Call the getEmail and get the payload ***/
        $resp = $this->email->getEmail();

        /*** Make assertions ***/
        $this->assertEquals(\MiamiOH\RESTng\App::API_UNAUTHORIZED, $resp->getStatus());
    }

    public function testGetEmailBadPidm()
    {
        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $pidmForThisTest = '1147436x';
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('pidm' => array($pidmForThisTest), 'token' => 'blahblah'));
        $this->email->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        //define the querry_all method for this test
        $this->dbh->method('queryall_array')
            ->willReturn(array());

        /*** Call the getEmail ***/
        try {
            $resp = $this->email->getEmail();
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), 'Pidms must be numeric. (' . $pidmForThisTest . ')');
        }
    }

    public function testGetEmailInvalidToken()
    {
        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('pidm' => array($this->mockDbRecordsSinglePidm[0]['goremal_pidm']), 'token' => 'blahblah'));
        $this->email->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            //->will($this->returnCallback(array($this, 'authorizedUser'))); //for an authorized user
            ->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        /*** Call the getEmail and get the payload ***/
        $resp = $this->email->getEmail();

        /*** Make assertions ***/
        $this->assertEquals(\MiamiOH\RESTng\App::API_UNAUTHORIZED, $resp->getStatus());
    }

    //helper methods
    public function authorizedUser()
    {
        return true;
    }

    public function notAuthorizedUser()
    {
        return false;
    }

    //private vars
    private $mockDbRecordsSinglePidm = array( //array of 3 database records
        array( //db record 1
            'goremal_pidm' => '1010101',
            'goremal_emal_code' => 'ADM',
            'goremal_email_address' => 'personaltestemail@gmail.com',
            'goremal_status_ind' => 'A',
            'goremal_preferred_ind' => 'N',
            'goremal_activity_date' => '18-MAR-15',
            'goremal_user_id' => 'PROCESS',
            'goremal_comment' => 'comment to add',
            'goremal_disp_web_ind' => 'N',
            'row_id' => 'asdfasdf'),
        array( //db record 2
            'goremal_pidm' => '1010101',
            'goremal_emal_code' => 'MAE',
            'goremal_email_address' => 'persont@miami.com',
            'goremal_status_ind' => 'A',
            'goremal_preferred_ind' => 'N',
            'goremal_activity_date' => '12-APR-12',
            'goremal_user_id' => 'BANNER',
            'goremal_comment' => 'alumni/invalid email',
            'goremal_disp_web_ind' => 'N',
            'row_id' => 'asdfasdf'),
        array( //db record 3
            'goremal_pidm' => '1010101',
            'goremal_emal_code' => 'MU',
            'goremal_email_address' => 'persont@miamioh.edu',
            'goremal_status_ind' => 'A',
            'goremal_preferred_ind' => 'N',
            'goremal_activity_date' => '31-OCT-14',
            'goremal_user_id' => 'JONESTE',
            'goremal_comment' => 'main university email address',
            'goremal_disp_web_ind' => 'Y',
            'row_id' => 'asdfasdf'),
    );
    private $mockDbRecordsMultiPidm = array( //array of 4 database records
        array( //db record 1
            'goremal_pidm' => '1010101',
            'goremal_emal_code' => 'ADM',
            'goremal_email_address' => 'personaltestemail@gmail.com',
            'goremal_status_ind' => 'A',
            'goremal_preferred_ind' => 'N',
            'goremal_activity_date' => '18-MAR-15',
            'goremal_user_id' => 'PROCESS',
            'goremal_comment' => 'comment to add',
            'goremal_disp_web_ind' => 'N',
            'row_id' => 'asdfasdf'),
        array( //db record 2
            'goremal_pidm' => '1010101',
            'goremal_emal_code' => 'MAE',
            'goremal_email_address' => 'persont@miami.com',
            'goremal_status_ind' => 'A',
            'goremal_preferred_ind' => 'N',
            'goremal_activity_date' => '12-APR-12',
            'goremal_user_id' => 'BANNER',
            'goremal_comment' => 'alumni/invalid email',
            'goremal_disp_web_ind' => 'N',
            'row_id' => 'asdfasdf'),
        array( //db record 3
            'goremal_pidm' => '1010101',
            'goremal_emal_code' => 'MU',
            'goremal_email_address' => 'persont@miamioh.edu',
            'goremal_status_ind' => 'A',
            'goremal_preferred_ind' => 'N',
            'goremal_activity_date' => '31-OCT-14',
            'goremal_user_id' => 'JONESTE',
            'goremal_comment' => 'main university email address',
            'goremal_disp_web_ind' => 'Y',
            'row_id' => 'asdfasdf'),
        array( //db record 4
            'goremal_pidm' => '9999436',
            'goremal_emal_code' => 'MU',
            'goremal_email_address' => 'anotherperson@miamioh.edu',
            'goremal_status_ind' => 'A',
            'goremal_preferred_ind' => 'N',
            'goremal_activity_date' => '31-OCT-11',
            'goremal_user_id' => 'JONESTE',
            'goremal_comment' => 'main university email address',
            'goremal_disp_web_ind' => 'Y',
            'row_id' => 'asdfasdf'),
    );

}