<?php
/*
-----------------------------------------------------------
FILE NAME: Email.class.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Erin Mills

DESCRIPTION:  The email service is designed to get, update and insert phone records to banner goremal table
When doing get, pidm or uniqueId is required
When doing post, pidm and sequence number is needed

De-dup: The service will first check if there are duplicate records before doing an insert, in the event that
it finds duplicate records, an error message will be returned to consumer app

Authorization: authentication and authorization are handled by authorization token. The Service will treat
phone data as public but when people insert new records, the consumer app would need update right of person
project in Authman

Post without sequence number: if the record being posted does not have duplicate in database, and the consumer
app did not provide seqence number, the service will treat this a new valiad phone number and will add up one
to the current sequence number and perform insert

Protected group: SPBPERS_CONFID_IND = 'Y'. We will not update/get/insert any data for this people. Error message will
not be sent out but the consumer app should know the feature of this service.

INPUT:
PARAMETERS: pidm, sequence number and uniqueId

ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

TABLE USAGE:
	GENERAL.GOREMAL (SELECT, INSERT)
	SATURN.SPBPERS (SELECT)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

09/25/2015               MILLSE
Description:  Initial Program

01/25/2019            RAJENDS
Added put for email

 */

namespace MiamiOH\PhpEmailService\Services;

use MiamiOH\RESTng\App;

class Email extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD'; //secure datasource

    //	Helper functions that were called by the frame work and create internal datasource and configuration objects
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /*
     * Get email records for a pidm
     */
    public function getEmail()
    { //read

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();
        //$additionalwhere = null;
        $pidms = array();

        //Confirm User Can actually Access this Information
        $user = $this->getApiUser();
        $authorized = $user->isAuthorized('WebServices', 'Person', 'All');
        if (!$authorized) {
            $authorized = $user->isAuthorized('WebServices', 'Person', 'view');
        }

        if (!$authorized) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);//401
            return $response;
        }
        //get parameters from user input (url)
        if ((isset($options['pidm']) && is_array($options['pidm']) && count($options['pidm']) > 0)
            //todo remove the uniqueid option
            || (isset($options['uniqueid']) && is_array($options['uniqueid']) && count($options['uniqueid']) > 0)
        ) { //if pidms or uniqueids are given

            //define the database handle
            $dbh = $this->database->getHandle($this->datasource_name);


            $intPidms = '';
            foreach ($options['pidm'] as $p) {
                $pidmIsNumeric = is_numeric($p);
                if ($pidmIsNumeric) {
                    $intPidms = $intPidms . $p . ',';
                } else {
                    throw new \Exception('Pidms must be numeric. (' . $p . ')');
                }
            }
            $intPidms = substr($intPidms, 0, strlen($intPidms) - 1);

            $queryString = "select
							  goremal_pidm,
							  goremal_emal_code,
							  goremal_email_address,
							  goremal_status_ind,
							  goremal_preferred_ind,
							  goremal_activity_date,
							  goremal_user_id,
							  goremal_comment,
							  rowidtochar(goremal.rowid) as row_id,
							  goremal_disp_web_ind
							from goremal, spbpers
							where goremal_pidm in (" . $intPidms . ")
							and spbpers_pidm = goremal_pidm
							and (spbpers_confid_ind = 'N' or spbpers_confid_ind is null)
							order by goremal_pidm";

            $results = $dbh->queryall_array($queryString);
            $payload = $this->buildGetQueryResults($results);

        } else {
            throw new \Exception('Error getting options or parameter: No pidms or uniqueids are available.');
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;

    }

    public function createEmail()
    { //post
        //get the response and request objects
        $response = $this->getResponse();
        $request = $this->getRequest();
        $incomingData = $request->getData();
        $payload = array();

        //set up some tracking variables
        $successfulPidms = array();
        $failurePidms = array();
        $successCount = 0;
        $failureCount = 0;
        $report = array();

        //Confirm User Can actually Access this Information
        $user = $this->getApiUser();
        $authorized = $user->isAuthorized('WebServices', 'Person', 'All');
        if (!$authorized) {
            $authorized = $user->isAuthorized('WebServices', 'Person', 'create');
        }

        if (!$authorized) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            $response->setPayload(array('body'=>'The consumer could not be authenticated or is not authorized to access the resource'));
            return $response;
        }

        $dbh = $this->database->getHandle($this->datasource_name);

        //loop through given data records
        foreach ($incomingData as $emailRecord) {
            //pull out the data from the $emailRecord for later use
            $pidm = $emailRecord['pidm'];
            $emailCode = $emailRecord['emailCode'];
            $emailAddress = $emailRecord['emailAddress'];
            $statusInd = $emailRecord['statusInd'];
            $preferredInd = $emailRecord['preferredInd'];
            $comment = $emailRecord['comment'];
            $dispWebInd = $emailRecord['dispWebInd'];

            //check to see if this record is in the database already
            $selectQuery = "select
								goremal_pidm,
								goremal_emal_code,
								goremal_email_address
							from goremal
							where goremal_pidm = ?
							and goremal_emal_code = ?
							and goremal_email_address = ?";

            $results = $dbh->queryall_array($selectQuery, $pidm, $emailCode, $emailAddress);

            if (!$results) {
                //check comment length and pidm for numerics before doing the insert.
                if (is_null($pidm) || is_null($emailCode) || is_null($emailAddress) || $pidm == '' || $emailCode == '' || $emailAddress == '') {
                    $report[] = array('pidm' => $pidm, 'code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => "Pidm, email code and email address must all be given.");
                    //array_push($failurePidms, array('pidm' => $pidm, 'message' => 'Pidm, email code and email address must all be given.'));
                    //$failureCount++;
                } else if (strlen($comment) > 60) {
                    $report[] = array('pidm' => $pidm, 'code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => "The comment given was too long. Comments can only be 60 characters long.");
                    //array_push($failurePidms, array('pidm' => $pidm, 'message' => 'The comment given was too long. Comments can only be 60 characters long.'));
                    //$failureCount++;
                } else if (!is_numeric($pidm)) {
                    $report[] = array('pidm' => $pidm, 'code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => "Pidm must be numeric.");
                    //array_push($failurePidms, array('pidm' => $pidm, 'message' => 'Pidm must be numeric.'));
                    //$failureCount++;
                } else {
                    //now, insert the record.
                    try {
                        $query = "
                        BEGIN
                            GB_EMAIL.P_CREATE(
                                p_pidm => :P_PIDM,
                                p_emal_code => :P_EMAL_CODE,
                                p_email_address => :P_EMAIL_ADDRESS,
                                p_status_ind => :P_STATUS_IND,
                                p_preferred_ind => :P_PREFERRED_IND,
                                p_user_id => :P_USER_ID,
                                p_comment => :P_COMMENT,
                                p_disp_web_ind => :P_DISP_WEB_IND,
                                p_data_origin => 'Banner',
                                p_rowid_out => :P_ROWID_OUT
                            );
                        END;";
                        $sth = $dbh->prepare($query);
                        $rowId = null;
                        $username = $user->getUsername();
                        $sth->bind_by_name(':P_PIDM', $pidm);
                        $sth->bind_by_name(':P_EMAL_CODE', $emailCode);
                        $sth->bind_by_name(':P_EMAIL_ADDRESS', $emailAddress);
                        $sth->bind_by_name(':P_STATUS_IND', $statusInd);
                        $sth->bind_by_name(':P_PREFERRED_IND', $preferredInd);
                        $sth->bind_by_name(':P_USER_ID', $username);
                        $sth->bind_by_name(':P_COMMENT', $comment);
                        $sth->bind_by_name(':P_DISP_WEB_IND', $dispWebInd);
                        $sth->bind_by_name(':P_ROWID_OUT', $rowId, 100);

                        $sth->execute();

                        $report[] = [
                            'id' => $rowId,
                            'pidm' => $pidm,
                            'code' => \MiamiOH\RESTng\App::API_CREATED,
                            'message' => "Email successfully inserted."
                        ];
                    } catch (\Exception $e) {
                        $report[] = array(
                            'pidm' => $pidm,
                            'code' => \MiamiOH\RESTng\App::API_FAILED,
                            'message' => "There was a database error when attempting to insert the record. Error: ". $e->getMessage()
                        );
                        array_push($failurePidms, array('pidm' => $pidm, 'message' => 'There was a database error when attempting to insert the record. Error: ' . $e->getMessage()));
                        $failureCount++;
                    }
                }

            } else {
                $report[] = array('pidm' => $pidm, 'code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => "A record was already found with this email: $emailAddress.");
                //array_push($failurePidms, array('pidm'=>$pidm, 'message'=>"A record was already found with this email: $emailAddress."));
                //$failureCount++;
            }
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($report);
        return $response;
    }

    public function updateEmail()
    {
        //get the response and request objects
        $response = $this->getResponse();
        $request = $this->getRequest();
        $incomingData = $request->getData();

        //set up some tracking variables
        //$successfulPidms = array();
        //$successCount = 0;
        $failurePidms = array();
        $failureCount = 0;
        $report = array();

        //Confirm User Can actually Access this Information
        $user = $this->getApiUser();
        $authorized = $user->isAuthorized('WebServices', 'Person', 'All');
        if (!$authorized) {
            $authorized = $user->isAuthorized('WebServices', 'Person', 'update');
        }

        if (!$authorized) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $response;
        }

        $dbh = $this->database->getHandle($this->datasource_name);

        //loop through given data records
        foreach ($incomingData as $emailRecord) {
            //check to see if this record is in the database already
            $selectQuery = "select
								goremal_pidm,
								goremal_emal_code,
								goremal_email_address
							from goremal
							where goremal_pidm = ?
							and goremal_emal_code = ?
							and goremal_email_address = ?";

            $results = $dbh->queryall_array($selectQuery, $emailRecord['pidm'], $emailRecord['oldEmailCode'], $emailRecord['oldEmailAddress']);

            if ($results) {
                //check required fields
                $report[]=$this->getApiUpdateMessage($dbh,$user,$emailRecord);

            } else {
                $report[] = array('pidm' => $emailRecord['pidm'], 'code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => "A record corresponding to the given pidm ".$emailRecord['pidm'].", old email code ".$emailRecord['oldEmailCode'].", old email address ".$emailRecord['oldEmailAddress']." does not exist in the database. Please create the record first.");
            }
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($report);
        return $response;
    }
    
    public function deleteEmail()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $id = $request->getResourceParam('id');

        $data = [];
        $status = App::API_OK;

        try {
            $query = '
                    declare
                    BEGIN
                    GB_EMAIL.P_DELETE(
                        p_pidm => NULL,
                        p_emal_code => NULL,
                        p_email_address => NULL,
                        p_rowid => :P_ROW_ID
                    );
                    END;';
            $dbh = $this->database->getHandle($this->datasource_name);
            $sth = $dbh->prepare($query);
            $sth->bind_by_name(':P_ROW_ID', $id);
            $sth->execute();
        } catch (\MiamiOH\RESTng\Exception\BadRequest $e) {
            $status = App::API_BADREQUEST;
            $data['message'] = $e->getMessage();
        } catch (\Exception $e) {
            $status = App::API_FAILED;
            $data['message'] = $e->getMessage();
        }

        $response->setPayload($data);
        $response->setStatus($status);

        return $response;
    }
    
    private function getApiUpdateMessage($dbh,$user,$emailRecord) {
        $message = $this->getUpdateErrorMessage($emailRecord);
        if($message == '') {
            //now, update the record.
            $updateQuery = "update goremal 
                                    set 
                                        goremal_emal_code = ? , 
                                        goremal_email_address = ? ,
                                        goremal_status_ind = ?,
                                        goremal_preferred_ind = ?,
                                        goremal_activity_date = SYSDATE,
                                        goremal_user_id = ?,
                                        goremal_comment = ?,
                                        goremal_disp_web_ind = ?,
                                        goremal_data_origin = ?
                                    where
                                        goremal_pidm = ?
                                        and goremal_emal_code = ?
                                        and goremal_email_address = ?";


            $results = $dbh->perform($updateQuery, $emailRecord['newEmailCode'], $emailRecord['newEmailAddress'], $emailRecord['statusInd'], $emailRecord['preferredInd'], $user->getUsername(), $emailRecord['comment'], $emailRecord['dispWebInd'], 'WebService', $emailRecord['pidm'], $emailRecord['oldEmailCode'], $emailRecord['oldEmailAddress']);

            if ($results == true) {//if there is no error, push the pidm onto the success stack.
                $report = array('pidm' => $emailRecord['pidm'], 'code' => \MiamiOH\RESTng\App::API_CREATED, 'message' => "Email updated successfully");
                //array_push($successfulPidms, $emailRecord['pidm']);
                //$successCount++;
            } else { //if there is an error after the update, then push the pidm onto the failure stack.
                $report = array('pidm' => $emailRecord['pidm'], 'code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => "There was a database error when attempting to update the record.");
                //array_push($failurePidms, array('pidm' => $emailRecord['pidm'], 'message' => 'There was a database error when attempting to update the record.'));
                //$failureCount++;
            }
        } else {
            $report = array('pidm' => $emailRecord['pidm'], 'code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => $message);
        }
        return $report;
    }
    
    private function getUpdateErrorMessage($emailRecord) {
        $message='';
        if (is_null($emailRecord['pidm']) || is_null($emailRecord['newEmailCode']) || is_null($emailRecord['newEmailAddress']) || is_null($emailRecord['oldEmailCode']) || is_null($emailRecord['oldEmailAddress'])|| $emailRecord['pidm'] == '' || $emailRecord['newEmailCode'] == '' || $emailRecord['newEmailAddress'] == '' || $emailRecord['oldEmailCode'] == '' || $emailRecord['oldEmailAddress'] == '') {
            $message = "Pidm, old email code, new email code, old email address and new email address are required.";
        } else if (is_null($emailRecord['statusInd']) || strlen($emailRecord['statusInd']) > 1) {
            $message = "Status indicator cannot be null and can be maximum 1 character long";
        } else if (is_null($emailRecord['preferredInd']) || strlen($emailRecord['preferredInd']) > 1) {
            $message = "Preferred indicator cannot be null and can be maximum 1 character long";
        } else if (strlen($emailRecord['comment']) > 60) {
            $message = "The comment given was too long. Comments can only be 60 characters long.";
        } else if (!is_numeric($emailRecord['pidm'])) {
            $message = "Pidm must be numeric.";
        } else if (strlen($emailRecord['oldEmailCode']) > 4 || strlen($emailRecord['newEmailCode']) > 4) {
            $message = "Email Code can only be 4 characters long";
        } else if (is_null($emailRecord['dispWebInd']) || strlen($emailRecord['dispWebInd']) > 1) {
            $message = "Display web indicator cannot be null and can be maximum 1 character long";
        }

        return $message;
    }
    
    private function buildGetQueryResults($queryResults)
    {
        $returnArray = array();
        $i = 0;

        foreach ($queryResults as $qr) {
            $formattedEmailRecord = array();
            $formattedEmailRecord['id'] = $qr['row_id'];
            $formattedEmailRecord['pidm'] = $qr['goremal_pidm'];
            $formattedEmailRecord['emailCode'] = $qr['goremal_emal_code'];
            $formattedEmailRecord['emailAddress'] = $qr['goremal_email_address'];
            $formattedEmailRecord['statusInd'] = $qr['goremal_status_ind'];
            $formattedEmailRecord['preferredInd'] = $qr['goremal_preferred_ind'];
            $formattedEmailRecord['activity_date'] = $qr['goremal_activity_date'];
            $formattedEmailRecord['user_id'] = $qr['goremal_user_id'];
            $formattedEmailRecord['comment'] = $qr['goremal_comment'];
            $formattedEmailRecord['dispWebInd'] = $qr['goremal_disp_web_ind'];
            $returnArray['emailRecord' . $i] = $formattedEmailRecord;
            $i++;
        }
        return $returnArray;
    }
}
