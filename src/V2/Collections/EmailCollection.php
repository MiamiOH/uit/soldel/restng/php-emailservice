<?php


namespace MiamiOH\PhpEmailService\V2\Collections;

class EmailCollection extends JsonableCollection
{
    /**
     * @var int|null
     */
    private $totalCount = null;

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        if ($this->totalCount) {
            return $this->totalCount;
        }

        return count($this);
    }

    /**
     * @param int $totalCount
     */
    public function setTotalCount(int $totalCount): void
    {
        $this->totalCount = $totalCount;
    }
}
