<?php


namespace MiamiOH\PhpEmailService\V2\Collections;

use Illuminate\Support\Collection;
use MiamiOH\PhpEmailService\V2\Utils\Jsonable;

class JsonableCollection extends Collection implements Jsonable
{
    public function toJsonArray(): array
    {
        $data = [];

        /** @var Jsonable $item */
        foreach ($this as $item) {
            $data[] = $item->toJsonArray();
        }

        return $data;
    }
}
