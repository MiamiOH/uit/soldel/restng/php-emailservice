<?php


namespace MiamiOH\PhpEmailService\V2\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class EloquentEmailType extends Model
{
    protected $table = 'gtvemal';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('default', function (Builder $query) {
            $query->select([
                'gtvemal_code',
                'gtvemal_desc',
            ]);
        });
    }

    public function getCode(): string
    {
        return $this->gtvemal_code;
    }

    public function getDescription(): string
    {
        return $this->gtvemal_desc;
    }
}
