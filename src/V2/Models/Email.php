<?php


namespace MiamiOH\PhpEmailService\V2\Models;

use Carbon\Carbon;
use MiamiOH\PhpEmailService\V2\Utils\Jsonable;

class Email implements Jsonable
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var int
     */
    private $pidm;
    /**
     * @var string
     */
    private $typeCode;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $email;
    /**
     * @var bool
     */
    private $isPreferred;
    /**
     * @var bool
     */
    private $isActive;
    /**
     * @var bool
     */
    private $isDisplayedOnWeb;
    /**
     * @var string|null
     */
    private $comment;
    /**
     * @var string
     */
    private $updatedBy;
    /**
     * @var Carbon
     */
    private $updatedAt;

    /**
     * Email constructor.
     * @param string $id
     * @param int $pidm
     * @param string $typeCode
     * @param string $type
     * @param string $email
     * @param bool $isPreferred
     * @param bool $isActive
     * @param bool $isDisplayedOnWeb
     * @param string|null $comment
     * @param string $updatedBy
     * @param Carbon $updatedAt
     */
    public function __construct(string $id, int $pidm, string $typeCode, string $type, string $email, bool $isPreferred, bool $isActive, bool $isDisplayedOnWeb, ?string $comment, string $updatedBy, Carbon $updatedAt)
    {
        $this->id = $id;
        $this->pidm = $pidm;
        $this->typeCode = $typeCode;
        $this->type = $type;
        $this->email = $email;
        $this->isPreferred = $isPreferred;
        $this->isActive = $isActive;
        $this->isDisplayedOnWeb = $isDisplayedOnWeb;
        $this->comment = $comment;
        $this->updatedBy = $updatedBy;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPidm(): int
    {
        return $this->pidm;
    }

    /**
     * @return string
     */
    public function getTypeCode(): string
    {
        return $this->typeCode;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function isPreferred(): bool
    {
        return $this->isPreferred;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return bool
     */
    public function isDisplayedOnWeb(): bool
    {
        return $this->isDisplayedOnWeb;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return string
     */
    public function getUpdatedBy(): string
    {
        return $this->updatedBy;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }

    public function toJsonArray(): array
    {
        return [
            'id' => $this->getId(),
            'pidm' => $this->getPidm(),
            'typeCode' => $this->getTypeCode(),
            'type' => $this->getType(),
            'email' => $this->getEmail(),
            'isPreferred' => $this->isPreferred(),
            'isActive' => $this->isActive(),
            'isDisplayedOnWeb' => $this->isDisplayedOnWeb(),
            'comment' => $this->getComment(),
            'updatedBy' => $this->getUpdatedBy(),
            'updatedAt' => $this->getUpdatedAt()->format('Y-m-d H:i:s'),
        ];
    }
}
