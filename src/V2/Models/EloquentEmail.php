<?php


namespace MiamiOH\PhpEmailService\V2\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class EloquentEmail extends Model
{
    protected $table = 'goremal';
    protected $dates = [
        'goremal_activity_date'
    ];

    public function type()
    {
        return $this->hasOne(EloquentEmailType::class, 'gtvemal_code', 'goremal_emal_code');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('default', function (Builder $query) {
            $query->selectRaw("
                rowidtochar(goremal.rowid) as row_id,
                goremal_pidm,
                goremal_emal_code,
                goremal_email_address,
                goremal_preferred_ind,
                goremal_status_ind,
                goremal_disp_web_ind,
                goremal_comment,
                goremal_user_id,
                goremal_activity_date,
                goremal_surrogate_id
            ")
                ->with(['type']);
        });
    }

    public function scopeById(Builder $query, string $id)
    {
        $query->where('rowid', '=', $id);
    }

    public function scopeByIds(Builder $query, array $ids)
    {
        $query->whereIn('rowid', $ids);
    }

    public function scopeByPidms(Builder $query, array $pidms)
    {
        $query->whereIn('goremal_pidm', $pidms);
    }

    public function scopeByTypeCodes(Builder $query, array $codes)
    {
        $query->whereIn('goremal_emal_code', $codes);
    }

    public function scopeByPreferredIndicator(Builder $query, string $ind)
    {
        $query->where('goremal_preferred_ind', '=', $ind);
    }

    public function scopeByStatusIndicator(Builder $query, string $ind)
    {
        $query->where('goremal_status_ind', '=', $ind);
    }

    public function scopeByDisplayWebIndicator(Builder $query, string $ind)
    {
        $query->where('goremal_disp_web_ind', '=', $ind);
    }

    public function getId(): string
    {
        return $this->row_id;
    }

    public function getPidm(): int
    {
        return $this->goremal_pidm;
    }

    public function getTypeCode(): string
    {
        return $this->goremal_emal_code;
    }

    public function getType(): string
    {
        return $this->type->getDescription();
    }

    public function getEmail(): string
    {
        return $this->goremal_email_address;
    }

    public function isPreferred(): bool
    {
        return $this->goremal_preferred_ind === 'Y';
    }

    public function isActive(): bool
    {
        return $this->goremal_status_ind === 'A';
    }

    public function isDisplayedOnWeb(): bool
    {
        return $this->goremal_disp_web_ind === 'Y';
    }

    public function getComment(): ?string
    {
        return $this->goremal_comment;
    }

    public function getUpdatedBy(): string
    {
        return $this->goremal_user_id;
    }

    public function getUpdatedAt(): Carbon
    {
        return $this->goremal_activity_date;
    }
}
