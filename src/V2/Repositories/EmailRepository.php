<?php


namespace MiamiOH\PhpEmailService\V2\Repositories;

use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Eloquent\Collection;
use MiamiOH\PhpEmailService\V2\Collections\EmailCollection;
use MiamiOH\PhpEmailService\V2\Models\EloquentEmail;
use MiamiOH\PhpEmailService\V2\Models\Email;
use MiamiOH\PhpEmailService\V2\Requests\CreateEmailRequest;
use MiamiOH\PhpEmailService\V2\Requests\SearchEmailRequest;
use MiamiOH\PhpEmailService\V2\Requests\UpdateEmailRequest;

class EmailRepository
{
    public const DATA_ORIGIN = 'RESTng Person Email V2 Service';

    /**
     * @var EloquentEmail
     */
    private $eloquentEmailModel;
    /**
     * @var ConnectionInterface
     */
    private $db;

    /**
     * EmailRepository constructor.
     * @param EloquentEmail $eloquentEmailModel
     * @param ConnectionInterface $db
     */
    public function __construct(EloquentEmail $eloquentEmailModel, ConnectionInterface $db)
    {
        $this->eloquentEmailModel = $eloquentEmailModel;
        $this->db = $db;
    }

    public function get(string $id): ?Email
    {
        $eloquentEmail = $this->eloquentEmailModel
            ->byId($id)
            ->first();

        if ($eloquentEmail === null) {
            return null;
        }

        return $this->constructEmail($eloquentEmail);
    }

    public function search(SearchEmailRequest $request): EmailCollection
    {
        $query = $this->eloquentEmailModel;

        if (($ids = $request->getIds()) !== null) {
            $query = $query->byIds($ids);
        }

        if (($pidms = $request->getPidms()) !== null) {
            $query = $query->byPidms($pidms);
        }

        if (($typeCodes = $request->getTypeCodes()) !== null) {
            $query = $query->byTypeCodes($typeCodes);
        }

        if (($isPreferred = $request->getIsPreferred()) !== null) {
            $query = $query->byPreferredIndicator($this->toPreferredInd($isPreferred));
        }

        if (($isActive = $request->getIsActive()) !== null) {
            $query = $query->byStatusIndicator($this->toStatusInd($isActive));
        }

        if (($isDisplayedOnWeb = $request->getIsDisplayedOnWeb()) !== null) {
            $query = $query->byDisplayWebIndicator($this->toDisplayWebInd($isDisplayedOnWeb));
        }

        $totalCount = $query->count();

        $eloquentEmails = $query
            ->orderBy('goremal_surrogate_id')
            ->offset($request->getOffset())
            ->limit($request->getLimit())
            ->get();

        return $this->constructEmails($eloquentEmails, $totalCount);
    }

    public function create(CreateEmailRequest $request): Email
    {
        $id = '';

        $this->db->executeProcedure('gb_email.p_create', [
            'p_pidm' => $request->getPidm(),
            'p_emal_code' => $request->getTypeCode(),
            'p_email_address' => $request->getEmail(),
            'p_status_ind' => $this->toStatusInd($request->isActive()),
            'p_preferred_ind' => $this->toPreferredInd($request->isPreferred()),
            'p_user_id' => strtoupper($request->getCreatedBy()),
            'p_comment' => $request->getComment(),
            'p_disp_web_ind' => $this->toDisplayWebInd($request->isDisplayedOnWeb()),
            'p_data_origin' => self::DATA_ORIGIN,
            'p_rowid_out' => [
                'value' => &$id,
                'length' => 1000
            ],
        ]);

        return $this->get($id);
    }

    public function update(UpdateEmailRequest $request): Email
    {
        $id = $request->getId();

        $this->db->executeProcedure('gb_email.p_update', [
            'p_pidm' => $request->getPidm(),
            'p_emal_code' => $request->getTypeCode(),
            'p_email_address' => $request->getEmail(),
            'p_status_ind' => $this->toStatusInd($request->isActive()),
            'p_preferred_ind' => $this->toPreferredInd($request->isPreferred()),
            'p_user_id' => strtoupper($request->getUpdatedBy()),
            'p_comment' => $request->getComment(),
            'p_disp_web_ind' => $this->toDisplayWebInd($request->isDisplayedOnWeb()),
            'p_data_origin' => self::DATA_ORIGIN,
            'p_rowid' => $id
        ]);

        return $this->get($id);
    }

    public function delete(string $id): void
    {
        $this->db->executeProcedure('gb_email.p_delete', [
            'p_pidm' => null,
            'p_emal_code' => null,
            'p_email_address' => null,
            'p_rowid' => $id,
        ]);
    }

    public function unitOfWork(callable $callback)
    {
        return $this->db->transaction($callback);
    }

    private function constructEmails(Collection $eloquentEmails, int $totalCount): EmailCollection
    {
        $emails = new EmailCollection();

        foreach ($eloquentEmails as $eloquentEmail) {
            $emails->push($this->constructEmail($eloquentEmail));
        }

        $emails->setTotalCount($totalCount);

        return $emails;
    }

    private function constructEmail(EloquentEmail $eloquentEmail): Email
    {
        return new Email(
            $eloquentEmail->getId(),
            $eloquentEmail->getPidm(),
            $eloquentEmail->getTypeCode(),
            $eloquentEmail->getType(),
            $eloquentEmail->getEmail(),
            $eloquentEmail->isPreferred(),
            $eloquentEmail->isActive(),
            $eloquentEmail->isDisplayedOnWeb(),
            $eloquentEmail->getComment(),
            strtolower($eloquentEmail->getUpdatedBy()),
            $eloquentEmail->getUpdatedAt()
        );
    }

    private function toStatusInd(bool $isActive): string
    {
        return $isActive ? 'A' : 'I';
    }

    private function toDisplayWebInd(bool $isDisplayedOnWeb): string
    {
        return $isDisplayedOnWeb ? 'Y' : 'N';
    }

    private function toPreferredInd(bool $isPreferred): string
    {
        return $isPreferred ? 'Y' : 'N';
    }
}
