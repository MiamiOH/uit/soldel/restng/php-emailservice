<?php

namespace MiamiOH\PhpEmailService\V2\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class EmailResourceProvider extends ResourceProvider
{
    /**
     * @var string
     */
    private $tag = 'Person';
    /**
     * @var string
     */
    private $serviceName = 'Person.Email.V2';

    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => 'Person.Email.V2',
            'type' => 'object',
            'properties' => [
                'id' => [
                    'type' => 'string',
                    'example' => 'AAAWVzABNAAGiRYAAZ',
                ],
                'pidm' => [
                    'type' => 'integer',
                    'example' => 1483943,
                ],
                'typeCode' => [
                    'type' => 'string',
                    'example' => 'ADM',
                ],
                'type' => [
                    'type' => 'string',
                    'example' => 'Admission Email',
                ],
                'email' => [
                    'type' => 'string',
                    'example' => 'john.doe@gmail.com',
                ],
                'isPreferred' => [
                    'type' => 'boolean',
                    'example' => true,
                ],
                'isActive' => [
                    'type' => 'boolean',
                    'example' => true,
                ],
                'isDisplayedOnWeb' => [
                    'type' => 'boolean',
                    'example' => true,
                ],
                'comment' => [
                    'type' => 'string',
                    'example' => 'Registrar collects the email from the student.',
                ],
                'updatedBy' => [
                    'type' => 'string',
                    'example' => 'jandm',
                ],
                'updatedAt' => [
                    'type' => 'string',
                    'example' => '2020-02-23 10:20:00',
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Person.Email.V2.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/Person.Email.V2'
            ]
        ]);

        $this->addDefinition([
            'name' => 'Person.Email.V2.Error',
            'type' => 'object',
            'properties' => [
                'message' => [
                    'type' => 'string'
                ],
                'errors' => [
                    'type' => 'array',
                    'items' => []
                ]
            ]
        ]);

        $this->addDefinition([
            'name' => 'Person.Email.V2.Create.Email.Request',
            'type' => 'object',
            'properties' => [
                'pidm' => [
                    'type' => 'integer',
                    'example' => 1829374,
                ],
                'typeCode' => [
                    'type' => 'string',
                    'example' => 'ADM',
                ],
                'email' => [
                    'type' => 'string',
                    'example' => 'john.doe@gmail.com',
                ],
                'isPreferred' => [
                    'type' => 'boolean',
                    'example' => true,
                ],
                'isForced' => [
                    'type' => 'boolean',
                    'example' => true,
                ],
                'isActive' => [
                    'type' => 'boolean',
                    'example' => true,
                ],
                'isDisplayedOnWeb' => [
                    'type' => 'boolean',
                    'example' => true,
                ],
                'comment' => [
                    'type' => 'string',
                    'example' => 'Registrar collects the email from the student.',
                ],
                'createdBy' => [
                    'type' => 'string',
                    'example' => 'ladmn',
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Person.Email.V2.Update.Email.Request',
            'type' => 'object',
            'properties' => [
                'typeCode' => [
                    'type' => 'string',
                    'example' => 'ADM',
                ],
                'email' => [
                    'type' => 'string',
                    'example' => 'john.doe@gmail.com',
                ],
                'isPreferred' => [
                    'type' => 'boolean',
                    'example' => true,
                ],
                'isForced' => [
                    'type' => 'boolean',
                    'example' => true,
                ],
                'isActive' => [
                    'type' => 'boolean',
                    'example' => true,
                ],
                'isDisplayedOnWeb' => [
                    'type' => 'boolean',
                    'example' => true,
                ],
                'comment' => [
                    'type' => 'string',
                    'example' => 'Registrar collects the email from the student.',
                ],
                'updatedBy' => [
                    'type' => 'string',
                    'example' => 'lanbdm',
                ],
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => $this->serviceName,
            'class' => 'MiamiOH\PhpEmailService\V2\Services\EmailService',
            'description' => 'Improved Email Service',
            'set' => [
                'dataSource' => [
                    'type' => 'service', 'name' => 'APIDataSource'
                ],
            ]
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'summary' => 'Get a single email by ID.',
            'description' => <<<EOF
##### Description

Get a single email object by ID (ROWID).

##### Authorizations:

The caller must have one of the following keys in the CAM application "WebServices" and category "Person":
   - All
   - all
   - View
   - view
   
##### URL Params

   - id: the ROWID of the record in the SPRADDR table.
EOF
            ,
            'name' => 'person.email.v2.get',
            'service' => $this->serviceName,
            'method' => 'get',
            'tags' => [$this->tag],
            'pattern' => '/person/email/v2/:id',
            'params' => [
                'id' => [
                    'description' => 'Email ID',
                    'type' => 'single',
                    'required' => true
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A single email.',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Person.Email.V2',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'Email does not exist.',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Person.Email.V2.Error',
                    ]
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'All',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'all',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'View',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'view',
                    ]
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'read',
            'summary' => 'Search emails.',
            'description' => <<<EOF
##### Description

Get a list of emails that matches the search criteria. Pagination is implemented in
this resource. The default page size is 10.

##### Authorizations:

The caller must have one of the following keys in the CAM application "WebServices" and category "Person":
   - All
   - all
   - View
   - view
EOF
            ,
            'name' => 'person.email.v2.search',
            'service' => $this->serviceName,
            'method' => 'search',
            'tags' => [$this->tag],
            'pattern' => '/person/email/v2',
            'isPageable' => true,
            'defaultPageLimit' => 10,
            'options' => [
                'ids' => [
                    'type' => 'list',
                    'description' => 'Email IDs (separated by comma)'
                ],
                'pidms' => [
                    'type' => 'list',
                    'description' => 'Email IDs (separated by comma)'
                ],
                'typeCodes' => [
                    'type' => 'list',
                    'description' => 'Email IDs (separated by comma)'
                ],
                'isPreferred' => [
                    'enum' => [
                        true,
                        false,
                    ],
                    'default' => null,
                    'description' => 'Whether the email is the preferred email.',
                ],
                'isActive' => [
                    'enum' => [
                        true,
                        false,
                    ],
                    'default' => null,
                    'description' => 'Whether the email is active'
                ],
                'isDisplayedOnWeb' => [
                    'enum' => [
                        true,
                        false,
                    ],
                    'default' => null,
                    'description' => 'Whether the email is displayed'
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of emails matched with search criteria.',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Person.Email.V2.Collection',
                    ]
                ],
                App::API_BADREQUEST => [
                    'description' => 'The request body is invalid',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Person.Email.V2.Error'
                    ]
                ],
                App::API_FAILED => [
                    'description' => 'Failed to search emails.',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Person.Email.V2.Error',
                    ]
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'All',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'all',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'View',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'view',
                    ]
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'create',
            'summary' => 'Create a new email record.',
            'description' => <<<EOF
##### Description

Create a single email record. 

##### Authorizations:

The caller must have one of the following keys in the CAM application "WebServices" and category "Person":
   - All
   - all
   - Create
   - create
   
##### Preferred Email

Banner only allows one email to be marked as preferred for a person. In case you want to
create a new preferred email and the person already has a preferred email. You can specify
**"isForced": true** in the request body. So that the process will lifted out the preferred
flag from the other record and marked the newly created record as preferred. When the operation
fails, the process will perform a rollback and the preferred flag will put back to the original record.
EOF
            ,
            'name' => 'person.email.v2.create',
            'service' => $this->serviceName,
            'method' => 'create',
            'tags' => [$this->tag],
            'pattern' => '/person/email/v2',
            'body' => [
                'required' => true,
                'description' => 'Create Email Request',
                'schema' => [
                    '$ref' => '#/definitions/Person.Email.V2.Create.Email.Request'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'The newly created email record.',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Person.Email.V2',
                    ]
                ],
                App::API_BADREQUEST => [
                    'description' => 'The request body is invalid',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Person.Email.V2.Error'
                    ]
                ],
                App::API_FAILED => [
                    'description' => 'Failed to create a new email.',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Person.Email.V2.Error',
                    ]
                ],
            ],
            'middleware' => [
                'authenticate' => [
                    'type' => 'token'
                ],
                'authorize' => [
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'All',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'all',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'create',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'Create',
                    ]
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'update',
            'summary' => 'Update an existing email record.',
            'description' => <<<EOF
##### Authorizations:

The caller must have one of the following keys in the CAM application "WebServices" and category "Person":
   - All
   - all
   - Update
   - update
   
##### Preferred Email

Banner only allows one email to be marked as preferred for a person. In case the person already has a preferred email
and you want to update an existing email to mark it as preferred. You can specify
**"isForced": true** in the request body. So that the process will lifted out the preferred
flag from the current preferred record and marked the newly updated record as preferred. When the operation
fails, the process will perform a rollback and the preferred flag will put back to the original record.
EOF
            ,
            'name' => 'person.email.v2.update',
            'service' => $this->serviceName,
            'method' => 'update',
            'tags' => [$this->tag],
            'pattern' => '/person/email/v2/:id',
            'params' => [
                'id' => [
                    'description' => 'Email ID',
                    'type' => 'single',
                    'required' => true
                ],
            ],
            'body' => [
                'required' => true,
                'description' => 'Update Email Request',
                'schema' => [
                    '$ref' => '#/definitions/Person.Email.V2.Update.Email.Request'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'The newly updated email record.',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Person.Email.V2',
                    ]
                ],
                App::API_BADREQUEST => [
                    'description' => 'The request body is invalid',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Person.Email.V2.Error'
                    ]
                ],
                App::API_FAILED => [
                    'description' => 'Failed to update an existing email.',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Person.Email.V2.Error',
                    ]
                ],
            ],
            'middleware' => [
                'authenticate' => [
                    'type' => 'token'
                ],
                'authorize' => [
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'All',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'all',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'update',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'Update',
                    ]
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'delete',
            'summary' => 'Delete an existing email record.',
            'description' => <<<EOF
##### URL Params

   - id: the ROWID of the record.

##### Authorizations:

The caller must have one of the following keys in the CAM application "WebServices" and category "Person":
   - All
   - all
   - Delete
   - delete
EOF
            ,
            'name' => 'person.email.v2.delete',
            'service' => $this->serviceName,
            'method' => 'delete',
            'tags' => [$this->tag],
            'pattern' => '/person/email/v2/:id',
            'params' => [
                'id' => [
                    'description' => 'Email ID',
                    'type' => 'single',
                    'required' => true
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'The newly updated email record.',
                    'returns' => [
                        'type' => 'array',
                        'items' => [],
                    ]
                ],
                App::API_FAILED => [
                    'description' => 'Failed to delete an existing email.',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Person.Email.V2.Error',
                    ]
                ],
            ],
            'middleware' => [
                'authenticate' => [
                    'type' => 'token'
                ],
                'authorize' => [
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'All',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'all',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'Delete',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'delete',
                    ]
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {
    }
}
