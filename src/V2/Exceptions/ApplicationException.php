<?php


namespace MiamiOH\PhpEmailService\V2\Exceptions;

class ApplicationException extends \Exception
{
    /**
     * @return array
     */
    public function getErrors(): array
    {
        $errors = [];

        $prev = $this->getPrevious();
        while (true) {
            if (!$prev) {
                break;
            }

            $errors[] = $prev->getMessage();
            $prev = $prev->getPrevious();
        }

        return $errors;
    }
}
