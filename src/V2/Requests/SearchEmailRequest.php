<?php


namespace MiamiOH\PhpEmailService\V2\Requests;

use MiamiOH\PhpEmailService\V2\Utils\Jsonable;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;

class SearchEmailRequest implements Jsonable
{
    /**
     * @var array|null
     */
    private $ids;
    /**
     * @var array|null
     */
    private $pidms;
    /**
     * @var array|null
     */
    private $typeCodes;
    /**
     * @var bool|null
     */
    private $isPreferred;
    /**
     * @var bool|null
     */
    private $isActive;
    /**
     * @var bool|null
     */
    private $isDisplayedOnWeb;
    /**
     * @var int
     */
    private $offset;
    /**
     * @var int
     */
    private $limit;

    /**
     * SearchEmailRequest constructor.
     * @param array|null $ids
     * @param array|null $pidms
     * @param array|null $typeCodes
     * @param bool|null $isPreferred
     * @param bool|null $isActive
     * @param bool|null $isDisplayedOnWeb
     * @param int $offset
     * @param int $limit
     */
    public function __construct(?array $ids, ?array $pidms, ?array $typeCodes, ?bool $isPreferred, ?bool $isActive, ?bool $isDisplayedOnWeb, int $offset, int $limit)
    {
        $this->ids = $ids;
        $this->pidms = $pidms;
        $this->typeCodes = $typeCodes;
        $this->isPreferred = $isPreferred;
        $this->isActive = $isActive;
        $this->isDisplayedOnWeb = $isDisplayedOnWeb;
        $this->offset = $offset;
        $this->limit = $limit;
    }

    public static function createFromArray(array $data, int $offset, int $limit): self
    {
        $validator = RESTngValidatorFactory::make($data, [
            'ids' => 'bail|nullable|array',
            'ids.*' => 'bail|required|string',
            'pidms' => 'bail|nullable|array',
            'pidms.*' => 'bail|nullable|integer',
            'typeCodes' => 'bail|nullable|array',
            'typeCodes.*' => 'bail|required|string|max:4',
            'isPreferred' => 'bail|nullable|in:true,false',
            'isActive' => 'bail|nullable|in:true,false',
            'isDisplayedOnWeb' => 'bail|nullable|in:true,false',
        ]);

        $validator->validate();

        $process = function (array $items) {
            return array_values(array_filter(array_unique(array_map(function (string $item) {
                return trim($item);
            }, $items)), function ($item) {
                return trim($item) !== '';
            }));
        };

        return new self(
            isset($data['ids']) ? $process($data['ids']) : null,
            isset($data['pidms']) ? array_map(function (string $pidm) {
                return intval($pidm);
            }, $process($data['pidms'])) : null,
            isset($data['typeCodes']) ? $process($data['typeCodes']) : null,
            isset($data['isPreferred']) ? $data['isPreferred'] === 'true' : null,
            isset($data['isActive']) ? $data['isActive'] === 'true' : null,
            isset($data['isDisplayedOnWeb']) ? $data['isDisplayedOnWeb'] === 'true' : null,
            $offset,
            $limit
        );
    }

    public static function ofPreferredForThePerson(int $pidm): self
    {
        return new self(
            null,
            [$pidm],
            null,
            true,
            null,
            null,
            0,
            1
        );
    }

    /**
     * @return array|null
     */
    public function getIds(): ?array
    {
        return $this->ids;
    }

    /**
     * @return array|null
     */
    public function getPidms(): ?array
    {
        return $this->pidms;
    }

    /**
     * @return array|null
     */
    public function getTypeCodes(): ?array
    {
        return $this->typeCodes;
    }

    /**
     * @return bool|null
     */
    public function getIsPreferred(): ?bool
    {
        return $this->isPreferred;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @return bool|null
     */
    public function getIsDisplayedOnWeb(): ?bool
    {
        return $this->isDisplayedOnWeb;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    public function toJsonArray(): array
    {
        return [
            'ids' => $this->getIds(),
            'pidms' => $this->getPidms(),
            'typeCodes' => $this->getTypeCodes(),
            'isPreferred' => $this->getIsPreferred(),
            'isActive' => $this->getIsActive(),
            'isDisplayedOnWeb' => $this->getIsDisplayedOnWeb(),
            'offset' => $this->getOffset(),
            'limit' => $this->getLimit(),
        ];
    }
}
