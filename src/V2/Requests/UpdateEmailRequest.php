<?php


namespace MiamiOH\PhpEmailService\V2\Requests;

use MiamiOH\PhpEmailService\V2\Models\Email;
use MiamiOH\PhpEmailService\V2\Utils\Jsonable;

class UpdateEmailRequest implements Jsonable
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var int
     */
    private $pidm;
    /**
     * @var string
     */
    private $typeCode;
    /**
     * @var string
     */
    private $email;
    /**
     * @var bool
     */
    private $isPreferred;
    /**
     * @var bool
     */
    private $isActive;
    /**
     * @var bool
     */
    private $isDisplayedOnWeb;
    /**
     * @var string|null
     */
    private $comment;
    /**
     * @var string
     */
    private $updatedBy;

    /**
     * UpdateEmailRequest constructor.
     * @param string $id
     * @param int $pidm
     * @param string $typeCode
     * @param string $email
     * @param bool $isPreferred
     * @param bool $isActive
     * @param bool $isDisplayedOnWeb
     * @param string|null $comment
     * @param string $updatedBy
     */
    public function __construct(string $id, int $pidm, string $typeCode, string $email, bool $isPreferred, bool $isActive, bool $isDisplayedOnWeb, ?string $comment, string $updatedBy)
    {
        $this->id = $id;
        $this->pidm = $pidm;
        $this->typeCode = $typeCode;
        $this->email = $email;
        $this->isPreferred = $isPreferred;
        $this->isActive = $isActive;
        $this->isDisplayedOnWeb = $isDisplayedOnWeb;
        $this->comment = $comment;
        $this->updatedBy = $updatedBy;
    }

    public static function create(Email $email, UpdateEmailRequestDTO $dto): self
    {
        return new self(
            $dto->getId(),
            $email->getPidm(),
            $dto->getTypeCode() ?? $email->getTypeCode(),
            $dto->getEmail() ?? $email->getEmail(),
            $dto->getIsPreferred() === null ? $email->isPreferred() : $dto->getIsPreferred(),
            $dto->getIsActive() === null ? $email->isActive() : $dto->getIsActive(),
            $dto->getIsDisplayedOnWeb() === null ? $email->isDisplayedOnWeb() : $dto->getIsDisplayedOnWeb(),
            $dto->isCommentUpdated() ? $dto->getComment() : $email->getComment(),
            $dto->getUpdatedBy()
        );
    }

    public static function ofTakingOffPreferredFlag(Email $email, string $updatedBy): self
    {
        return new self(
            $email->getId(),
            $email->getPidm(),
            $email->getTypeCode(),
            $email->getEmail(),
            false,
            $email->isActive(),
            $email->isDisplayedOnWeb(),
            $email->getComment(),
            $updatedBy
        );
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPidm(): int
    {
        return $this->pidm;
    }

    /**
     * @return string
     */
    public function getTypeCode(): string
    {
        return $this->typeCode;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function isPreferred(): bool
    {
        return $this->isPreferred;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return bool
     */
    public function isDisplayedOnWeb(): bool
    {
        return $this->isDisplayedOnWeb;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return string
     */
    public function getUpdatedBy(): string
    {
        return $this->updatedBy;
    }

    public function toJsonArray(): array
    {
        return [
            'id' => $this->getId(),
            'pidm' => $this->getPidm(),
            'typeCode' => $this->getTypeCode(),
            'email' => $this->getEmail(),
            'isPreferred' => $this->isPreferred(),
            'isActive' => $this->isActive(),
            'isDisplayedOnWeb' => $this->isDisplayedOnWeb(),
            'comment' => $this->getComment(),
            'updatedBy' => $this->getUpdatedBy(),
        ];
    }
}
