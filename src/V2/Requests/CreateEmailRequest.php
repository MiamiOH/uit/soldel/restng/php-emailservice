<?php


namespace MiamiOH\PhpEmailService\V2\Requests;

use MiamiOH\PhpEmailService\V2\Utils\Jsonable;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;

class CreateEmailRequest implements Jsonable
{
    /**
     * @var int
     */
    private $pidm;
    /**
     * @var string
     */
    private $typeCode;
    /**
     * @var string
     */
    private $email;
    /**
     * @var bool
     */
    private $isPreferred;
    /**
     * @var bool
     */
    private $isActive;
    /**
     * @var bool
     */
    private $isDisplayedOnWeb;
    /**
     * @var bool
     */
    private $isForced;
    /**
     * @var string|null
     */
    private $comment;
    /**
     * @var string
     */
    private $createdBy;

    /**
     * CreateEmailRequest constructor.
     * @param int $pidm
     * @param string $typeCode
     * @param string $email
     * @param bool $isPreferred
     * @param bool $isActive
     * @param bool $isDisplayedOnWeb
     * @param bool $isForced
     * @param string|null $comment
     * @param string $createdBy
     */
    public function __construct(int $pidm, string $typeCode, string $email, bool $isPreferred, bool $isActive, bool $isDisplayedOnWeb, bool $isForced, ?string $comment, string $createdBy)
    {
        $this->pidm = $pidm;
        $this->typeCode = $typeCode;
        $this->email = $email;
        $this->isPreferred = $isPreferred;
        $this->isActive = $isActive;
        $this->isDisplayedOnWeb = $isDisplayedOnWeb;
        $this->isForced = $isForced;
        $this->comment = $comment;
        $this->createdBy = $createdBy;
    }

    public static function createFromArray(array $data): self
    {
        $validator = RESTngValidatorFactory::make($data, [
            'pidm' => 'bail|required|integer',
            'typeCode' => 'bail|required|string|max:4',
            'email' => 'bail|required|string',
            'isPreferred' => 'bail|nullable|boolean',
            'isForced' => 'bail|nullable|boolean',
            'isActive' => 'bail|nullable|boolean',
            'isDisplayedOnWeb' => 'bail|nullable|boolean',
            'comment' => 'bail|nullable|string',
            'createdBy' => 'bail|required|string',
        ]);

        $validator->validate();

        return new self(
            $data['pidm'],
            $data['typeCode'],
            $data['email'],
            $data['isPreferred'] ?? false,
            $data['isActive'] ?? true,
            $data['isDisplayedOnWeb'] ?? true,
            $data['isForced'] ?? false,
            $data['comment'] ?? null,
            $data['createdBy']
        );
    }

    /**
     * @return int
     */
    public function getPidm(): int
    {
        return $this->pidm;
    }

    /**
     * @return string
     */
    public function getTypeCode(): string
    {
        return $this->typeCode;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function isPreferred(): bool
    {
        return $this->isPreferred;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return bool
     */
    public function isDisplayedOnWeb(): bool
    {
        return $this->isDisplayedOnWeb;
    }

    /**
     * @return bool
     */
    public function isForced(): bool
    {
        return $this->isForced;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    public function toJsonArray(): array
    {
        return [
            'pidm' => $this->getPidm(),
            'typeCode' => $this->getTypeCode(),
            'email' => $this->getEmail(),
            'isPreferred' => $this->isPreferred(),
            'isActive' => $this->isActive(),
            'isDisplayedOnWeb' => $this->isDisplayedOnWeb(),
            'isForced' => $this->isForced(),
            'comment' => $this->getComment(),
            'createdBy' => $this->getCreatedBy(),
        ];
    }
}
