<?php


namespace MiamiOH\PhpEmailService\V2\Requests;

use MiamiOH\PhpEmailService\V2\Utils\Jsonable;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;

class UpdateEmailRequestDTO implements Jsonable
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string|null
     */
    private $typeCode;
    /**
     * @var string|null
     */
    private $email;
    /**
     * @var bool|null
     */
    private $isPreferred;
    /**
     * @var bool|null
     */
    private $isActive;
    /**
     * @var bool|null
     */
    private $isDisplayedOnWeb;
    /**
     * @var bool
     */
    private $isForced;
    /**
     * @var string|null
     */
    private $comment;
    /**
     * @var bool
     */
    private $isCommentUpdated;
    /**
     * @var string
     */
    private $updatedBy;

    /**
     * UpdateEmailRequest constructor.
     * @param string $id
     * @param string|null $typeCode
     * @param string|null $email
     * @param bool|null $isPreferred
     * @param bool|null $isActive
     * @param bool|null $isDisplayedOnWeb
     * @param bool $isForced
     * @param string|null $comment
     * @param bool $isCommentUpdated
     * @param string $updatedBy
     */
    public function __construct(string $id, ?string $typeCode, ?string $email, ?bool $isPreferred, ?bool $isActive, ?bool $isDisplayedOnWeb, bool $isForced, ?string $comment, bool $isCommentUpdated, string $updatedBy)
    {
        $this->id = $id;
        $this->typeCode = $typeCode;
        $this->email = $email;
        $this->isPreferred = $isPreferred;
        $this->isActive = $isActive;
        $this->isDisplayedOnWeb = $isDisplayedOnWeb;
        $this->isForced = $isForced;
        $this->comment = $comment;
        $this->isCommentUpdated = $isCommentUpdated;
        $this->updatedBy = $updatedBy;
    }

    public static function createFromArray(array $data): self
    {
        $validator = RESTngValidatorFactory::make($data, [
            'id' => 'bail|required|string',
            'typeCode' => 'bail|required_without_all:email,isPreferred,isActive,isDisplayedOnWeb,comment|nullable|string|max:4',
            'email' => 'bail|required_without_all:typeCode,isPreferred,isActive,isDisplayedOnWeb,comment|nullable|string',
            'isPreferred' => 'bail|required_without_all:typeCode,email,isActive,isDisplayedOnWeb,comment|nullable|boolean',
            'isActive' => 'bail|required_without_all:typeCode,email,isPreferred,isDisplayedOnWeb,comment|nullable|boolean',
            'isForced' => 'bail|nullable|boolean',
            'isDisplayedOnWeb' => 'bail|required_without_all:typeCode,email,isPreferred,isActive,comment|nullable|boolean',
            'comment' => 'bail|required_without_all:typeCode,email,isPreferred,isActive,isDisplayedOnWeb|nullable|string',
            'updatedBy' => 'bail|required|string',
        ]);

        $validator->validate();

        return new self(
            $data['id'],
            $data['typeCode'] ?? null,
            $data['email'] ?? null,
            isset($data['isPreferred']) ? $data['isPreferred'] : null,
            isset($data['isActive']) ? $data['isActive'] : null,
            isset($data['isDisplayedOnWeb']) ? $data['isDisplayedOnWeb'] : null,
            $data['isForced'] ?? false,
            $data['comment'] ?? null,
            array_key_exists('comment', $data),
            $data['updatedBy']
        );
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTypeCode(): ?string
    {
        return $this->typeCode;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return bool|null
     */
    public function getIsPreferred(): ?bool
    {
        return $this->isPreferred;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @return bool|null
     */
    public function getIsDisplayedOnWeb(): ?bool
    {
        return $this->isDisplayedOnWeb;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return bool
     */
    public function isForced(): bool
    {
        return $this->isForced;
    }

    /**
     * @return bool
     */
    public function isCommentUpdated(): bool
    {
        return $this->isCommentUpdated;
    }

    /**
     * @return string
     */
    public function getUpdatedBy(): string
    {
        return $this->updatedBy;
    }

    public function toJsonArray(): array
    {
        return [
            'id' => $this->getId(),
            'typeCode' => $this->getTypeCode(),
            'email' => $this->getEmail(),
            'isPreferred' => $this->getIsPreferred(),
            'isActive' => $this->getIsActive(),
            'isDisplayedOnWeb' => $this->getIsDisplayedOnWeb(),
            'isForced' => $this->isForced(),
            'comment' => $this->getComment(),
            'isCommentUpdated' => $this->isCommentUpdated(),
            'updatedBy' => $this->getUpdatedBy(),
        ];
    }
}
