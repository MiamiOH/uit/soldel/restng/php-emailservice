<?php


namespace MiamiOH\PhpEmailService\V2\Utils;

interface Jsonable
{
    public function toJsonArray(): array;
}
