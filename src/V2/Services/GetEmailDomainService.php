<?php


namespace MiamiOH\PhpEmailService\V2\Services;

use MiamiOH\PhpEmailService\V2\Collections\EmailCollection;
use MiamiOH\PhpEmailService\V2\Exceptions\ApplicationException;
use MiamiOH\PhpEmailService\V2\Models\Email;
use MiamiOH\PhpEmailService\V2\Requests\SearchEmailRequest;

class GetEmailDomainService extends BaseDomainService
{
    /**
     * @param string $id
     * @return Email
     * @throws ApplicationException
     */
    public function get(string $id): Email
    {
        try {
            $email = $this->getEmailRepository()->get($id);
        } catch (\Exception $e) {
            throw $this->processException('Failed to get email by ID.', $e);
        }

        if ($email === null) {
            throw new ApplicationException(sprintf('Email (ID: %s) does not exist.', $id));
        }

        return $email;
    }

    /**
     * @param SearchEmailRequest $request
     * @return EmailCollection
     * @throws ApplicationException
     */
    public function search(SearchEmailRequest $request): EmailCollection
    {
        try {
            return $this->getEmailRepository()->search($request);
        } catch (\Exception $e) {
            throw $this->processException('Failed to search emails.', $e);
        }
    }
}
