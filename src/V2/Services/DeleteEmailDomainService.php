<?php


namespace MiamiOH\PhpEmailService\V2\Services;

use MiamiOH\PhpEmailService\V2\Exceptions\ApplicationException;

class DeleteEmailDomainService extends BaseDomainService
{
    /**
     * @param string $id
     * @throws ApplicationException
     */
    public function delete(string $id): void
    {
        try {
            $this->getEmailRepository()->delete($id);
        } catch (\Exception $e) {
            throw $this->processException('Failed to delete an existing email.', $e);
        }
    }
}
