<?php


namespace MiamiOH\PhpEmailService\V2\Services;

use MiamiOH\PhpEmailService\V2\Exceptions\ApplicationException;
use MiamiOH\PhpEmailService\V2\Models\Email;
use MiamiOH\PhpEmailService\V2\Repositories\EmailRepository;
use MiamiOH\PhpEmailService\V2\Requests\SearchEmailRequest;
use MiamiOH\PhpEmailService\V2\Requests\UpdateEmailRequest;
use MiamiOH\PhpEmailService\V2\Requests\UpdateEmailRequestDTO;

class UpdateEmailDomainService extends BaseDomainService
{
    /**
     * @var GetEmailDomainService
     */
    private $getEmailDomainService;

    public function __construct(EmailRepository $emailRepository, GetEmailDomainService $getEmailDomainService)
    {
        parent::__construct($emailRepository);
        $this->getEmailDomainService = $getEmailDomainService;
    }

    /**
     * @param UpdateEmailRequestDTO $request
     * @return Email
     * @throws ApplicationException
     */
    public function update(UpdateEmailRequestDTO $request): Email
    {
        try {
            $email = $this->getEmailDomainService->get($request->getId());

            $update = function () use ($email, $request) {
                return $this->getEmailRepository()->update(UpdateEmailRequest::create(
                    $email,
                    $request
                ));
            };

            if (!$request->getIsPreferred()
                || $request->getIsPreferred() === $email->isPreferred()
                || !$request->isForced()) {
                return $update();
            }

            /** @var Email|null $preferredEmail */
            $preferredEmail = $this->getEmailDomainService->search(SearchEmailRequest::ofPreferredForThePerson($email->getPidm()))[0] ?? null;

            if (!$preferredEmail) {
                return $update();
            }

            return $this->getEmailRepository()->unitOfWork(function () use ($preferredEmail, $request, $update) {
                $this->getEmailRepository()->update(UpdateEmailRequest::ofTakingOffPreferredFlag(
                    $preferredEmail,
                    $request->getUpdatedBy()
                ));

                return $update();
            });
        } catch (\Exception $e) {
            throw $this->processException('Failed to update an existing email record.', $e);
        }
    }
}
