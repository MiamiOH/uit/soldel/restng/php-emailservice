<?php


namespace MiamiOH\PhpEmailService\V2\Services;

use Illuminate\Validation\ValidationException;
use MiamiOH\PhpEmailService\V2\Exceptions\ApplicationException;
use MiamiOH\PhpEmailService\V2\Models\EloquentEmail;
use MiamiOH\PhpEmailService\V2\Repositories\EmailRepository;
use MiamiOH\PhpEmailService\V2\Requests\CreateEmailRequest;
use MiamiOH\PhpEmailService\V2\Requests\SearchEmailRequest;
use MiamiOH\PhpEmailService\V2\Requests\UpdateEmailRequestDTO;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;

class EmailService extends Service
{
    /**
     * @var GetEmailDomainService
     */
    private $getEmailDomainService;
    /**
     * @var CreateEmailDomainService
     */
    private $createEmailDomainService;
    /**
     * @var UpdateEmailDomainService
     */
    private $updateEmailDomainService;
    /**
     * @var DeleteEmailDomainService
     */
    private $deleteEmailDomainService;

    /**
     * EmailService constructor.
     * @param GetEmailDomainService|null $getEmailDomainService
     * @param CreateEmailDomainService|null $createEmailDomainService
     * @param UpdateEmailDomainService|null $updateEmailDomainService
     * @param DeleteEmailDomainService|null $deleteEmailDomainService
     */
    public function __construct(
        GetEmailDomainService $getEmailDomainService = null,
        CreateEmailDomainService $createEmailDomainService = null,
        UpdateEmailDomainService $updateEmailDomainService = null,
        DeleteEmailDomainService $deleteEmailDomainService = null
    ) {
        if ($getEmailDomainService) {
            $this->getEmailDomainService = $getEmailDomainService;
        }
        if ($createEmailDomainService) {
            $this->createEmailDomainService = $createEmailDomainService;
        }
        if ($updateEmailDomainService) {
            $this->updateEmailDomainService = $updateEmailDomainService;
        }
        if ($deleteEmailDomainService) {
            $this->deleteEmailDomainService = $deleteEmailDomainService;
        }
    }

    public function setDataSource(DataSource $ds): void
    {
        RESTngEloquentFactory::boot($ds->getDataSource('MUWS_SEC_PROD'));

        $emailRepository = new EmailRepository(
            new EloquentEmail(),
            EloquentEmail::getConnectionResolver()->connection()
        );
        $this->getEmailDomainService = new GetEmailDomainService($emailRepository);
        $this->createEmailDomainService = new CreateEmailDomainService($emailRepository, $this->getEmailDomainService);
        $this->updateEmailDomainService = new UpdateEmailDomainService($emailRepository, $this->getEmailDomainService);
        $this->deleteEmailDomainService = new DeleteEmailDomainService($emailRepository);
    }

    public function get(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $payload = [];
        $status = App::API_OK;

        $id = $request->getResourceParam('id');

        try {
            $email = $this->getEmailDomainService->get($id);
            $payload = $email->toJsonArray();
        } catch (ApplicationException $e) {
            $payload['message'] = $e->getMessage();
            $payload['errors'] = $e->getErrors();
            $status = App::API_NOTFOUND;
        }

        $response->setPayload($payload);
        $response->setStatus($status);
        return $response;
    }

    public function search(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $payload = [];
        $status = App::API_OK;

        $options = $request->getOptions();
        try {
            $searchEmailRequest = SearchEmailRequest::createFromArray(
                $options,
                $request->getOffset() - 1,
                $request->getLimit()
            );
            $emails = $this->getEmailDomainService->search($searchEmailRequest);
            $payload = $emails->toJsonArray();
            $response->setTotalObjects($emails->getTotalCount());
        } catch (ValidationException $e) {
            $payload = [
                'message' => $e->getMessage(),
                'errors' => $this->getErrors($e)
            ];
            $status = App::API_BADREQUEST;
        } catch (ApplicationException $e) {
            $payload['message'] = $e->getMessage();
            $payload['errors'] = $e->getErrors();
            $status = App::API_FAILED;
        }

        $response->setPayload($payload);
        $response->setStatus($status);
        return $response;
    }

    public function create(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $payload = [];
        $status = App::API_OK;

        $user = $this->getApiUser()->getUsername();
        $body = $request->getData();
        $body['createdBy'] = $user;
        try {
            $createEmailRequest = CreateEmailRequest::createFromArray($body);
            $email = $this->createEmailDomainService->create($createEmailRequest);
            $payload = $email->toJsonArray();
        } catch (ValidationException $e) {
            $payload = [
                'message' => $e->getMessage(),
                'errors' => $this->getErrors($e)
            ];
            $status = App::API_BADREQUEST;
        } catch (ApplicationException $e) {
            $payload['message'] = $e->getMessage();
            $payload['errors'] = $e->getErrors();
            $status = App::API_FAILED;
        }

        $response->setPayload($payload);
        $response->setStatus($status);
        return $response;
    }

    public function update(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $payload = [];
        $status = App::API_OK;

        $id = $request->getResourceParam('id');
        $user = $this->getApiUser()->getUsername();
        $body = $request->getData();
        $body['updatedBy'] = $user;
        $body['id'] = $id;

        try {
            $updateEmailRequest = UpdateEmailRequestDTO::createFromArray($body);
            $email = $this->updateEmailDomainService->update($updateEmailRequest);
            $payload = $email->toJsonArray();
        } catch (ValidationException $e) {
            $payload = [
                'message' => $e->getMessage(),
                'errors' => $this->getErrors($e)
            ];
            $status = App::API_BADREQUEST;
        } catch (ApplicationException $e) {
            $payload['message'] = $e->getMessage();
            $payload['errors'] = $e->getErrors();
            $status = App::API_FAILED;
        }

        $response->setPayload($payload);
        $response->setStatus($status);
        return $response;
    }

    public function delete(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $payload = [];
        $status = App::API_OK;

        $id = $request->getResourceParam('id');
        try {
            $this->deleteEmailDomainService->delete($id);
        } catch (ApplicationException $e) {
            $payload['message'] = $e->getMessage();
            $payload['errors'] = $e->getErrors();
            $status = App::API_FAILED;
        }

        $response->setPayload($payload);
        $response->setStatus($status);
        return $response;
    }

    private function getErrors(ValidationException $e): array
    {
        $errorBag = $e->errors();
        $errors = [];
        foreach ($errorBag as $reasons) {
            foreach ($reasons as $reason) {
                $errors[] = $reason;
            }
        }
        return $errors;
    }
}
