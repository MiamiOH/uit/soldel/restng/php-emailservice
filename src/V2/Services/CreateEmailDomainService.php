<?php


namespace MiamiOH\PhpEmailService\V2\Services;

use MiamiOH\PhpEmailService\V2\Exceptions\ApplicationException;
use MiamiOH\PhpEmailService\V2\Models\Email;
use MiamiOH\PhpEmailService\V2\Repositories\EmailRepository;
use MiamiOH\PhpEmailService\V2\Requests\CreateEmailRequest;
use MiamiOH\PhpEmailService\V2\Requests\SearchEmailRequest;
use MiamiOH\PhpEmailService\V2\Requests\UpdateEmailRequest;

class CreateEmailDomainService extends BaseDomainService
{
    /**
     * @var GetEmailDomainService
     */
    private $getEmailDomainService;

    public function __construct(EmailRepository $emailRepository, GetEmailDomainService $getEmailDomainService)
    {
        parent::__construct($emailRepository);
        $this->getEmailDomainService = $getEmailDomainService;
    }

    /**
     * @param CreateEmailRequest $request
     * @return Email
     * @throws ApplicationException
     */
    public function create(CreateEmailRequest $request): Email
    {
        try {
            $create = function () use ($request) {
                return $this->getEmailRepository()->create($request);
            };

            if (!$request->isPreferred() || !$request->isForced()) {
                return $create();
            }

            /** @var Email|null $preferredEmail */
            $preferredEmail = $this->getEmailDomainService->search(SearchEmailRequest::ofPreferredForThePerson($request->getPidm()))[0] ?? null;


            if (!$preferredEmail) {
                return $create();
            }

            return $this->getEmailRepository()->unitOfWork(function () use ($preferredEmail, $request, $create) {
                $this->getEmailRepository()->update(UpdateEmailRequest::ofTakingOffPreferredFlag(
                    $preferredEmail,
                    $request->getCreatedBy()
                ));

                return $create();
            });
        } catch (\Exception $e) {
            throw $this->processException('Failed to create a new email record.', $e);
        }
    }
}
