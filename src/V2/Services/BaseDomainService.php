<?php


namespace MiamiOH\PhpEmailService\V2\Services;

use MiamiOH\PhpEmailService\V2\Exceptions\ApplicationException;
use MiamiOH\PhpEmailService\V2\Repositories\EmailRepository;

abstract class BaseDomainService
{
    /**
     * @var EmailRepository
     */
    private $emailRepository;

    /**
     * EmailDomainService constructor.
     * @param EmailRepository $emailRepository
     */
    public function __construct(EmailRepository $emailRepository)
    {
        $this->emailRepository = $emailRepository;
    }

    /**
     * @return EmailRepository
     */
    public function getEmailRepository(): EmailRepository
    {
        return $this->emailRepository;
    }

    /**
     * @param \Exception $e
     * @return ApplicationException
     */
    protected function processException(string $message, \Exception $e): ApplicationException
    {
        $matches = [];
        preg_match(
            '/Error Message : ORA-\\d+: ::(.+?)::|Error Message : ORA-\\d+: (.+)/',
            $e->getMessage(),
            $matches
        );

        if (isset($matches[1]) || isset($matches[2])) {
            $msg = $matches[1];
            if ($msg === '') {
                $msg = $matches[2];
            }
            return new ApplicationException($msg);
        }

        return new ApplicationException($message, 0, $e);
    }
}
