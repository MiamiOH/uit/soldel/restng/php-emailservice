<?php

namespace MiamiOH\PhpEmailService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class EmailResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'Person',
            'description' => 'Resources for data about people'
        ));


        $this->addDefinition(array(
            'name' => 'Person.Email.PostResponse',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'integer',
                ),
                'code' => array(
                    'type' => 'string',
                ),
                'message' => array(
                    'type' => 'string',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'Person.Email.PostResponse.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Email.PostResponse'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Email.PatchResponse',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'integer',
                ),
                'oldEmailCode' => array(
                    'type' => 'string',
                ),
                'newEmailCode' => array(
                    'type' => 'string',
                ),
                'oldEmailAddress' => array(
                    'type' => 'string',
                ),
                'newEmailAddress' => array(
                    'type' => 'string',
                ),
                'statusInd' => array(
                    'type' => 'string',
                ),
                'preferredInd' => array(
                    'type' => 'string',
                ),
                'comment' => array(
                    'type' => 'string',
                ),
                'dispWebInd' => array(
                    'type' => 'string',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'Person.Email.PatchResponse.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Email.PatchResponse'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Email',
            'type' => 'object',
            'properties' => array(
                'id' => [
                    'type' => 'string'
                ],
                'pidm' => array(
                    'type' => 'integer',
                ),
                'emailCode' => array(
                    'type' => 'string',
                ),
                'emailAddress' => array(
                    'type' => 'string',
                ),
                'statusInd' => array(
                    'type' => 'string',
                ),
                'preferredInd' => array(
                    'type' => 'string',
                ),
                'comment' => array(
                    'type' => 'string',
                ),
                'dispWebInd' => array(
                    'type' => 'string',
                ),
                'activityDate' => array(
                    'type' => 'string'
                ),
                'userId' => array(
                    'type' => 'string'
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Email.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Email'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(

            'name' => 'Email',
            'class' => 'MiamiOH\PhpEmailService\Services\Email',
            'description' => 'Information about a persons email',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'dataSource' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(

            'action' => 'create',
            'name' => 'person.email.post',
            'description' => 'Create new email with pidm',
            'pattern' => '/person/email/v1',
            'service' => 'Email',
            'method' => 'createEmail',
            'tags' => array('Person'),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
            ),
            'returnType' => 'collection',
            'body' => array(
                'description' => 'An email object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Person.Email.Collection'
                )
            ),
            'responses' => array(
                App::API_CREATED => array(
                    'description' => 'Status code indicating status of email creation',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.Email.PostResponse.Collection',
                    )
                ),
            ),
            
        ));

        $this->addResource(array(

            'action' => 'update',
            'name' => 'person.email.patch',
            'description' => 'Update email with pidm',
            'pattern' => '/person/email/v1',
            'service' => 'Email',
            'method' => 'updateEmail',
            'tags' => array('Person'),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
            ),
            'returnType' => 'collection',
            'body' => array(
                'description' => 'An email object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Person.Email.PatchResponse.Collection'
                )
            ),
            'responses' => array(
                App::API_CREATED => array(
                    'description' => 'Status code indicating email record update',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.Email.PatchResponse.Collection',
                    )
                ),
            ),
        ));

        $this->addResource(array(

            'action' => 'read',
            'name' => 'person.email.get',
            'description' => 'Read a person\'s email',
            'pattern' => '/person/email/v1',
            'service' => 'Email',
            'method' => 'getEmail',
            'isPageable' => false,
            'tags' => array('Person'),
            'returnType' => 'collection',
            'options' => array(
                'pidm' => array('type' => 'list', 'description' => 'use pidm to get email records'),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A collection of emails',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.Email.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'delete',
            'name' => 'person.email.delete',
            'description' => 'Delete a person email',
            'pattern' => '/person/email/v1/:id',
            'params' => array(
                'id' => array(
                    'description' => 'Email ID'
                ),
            ),
            'returnType' => 'collection',
            'service' => 'Email',
            'method' => 'deleteEmail',
            'tags' => array('Person'),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'All',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person',
                        'key' => 'delete',
                    ),
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Status code indicating status of email deletion'
                ),
            ),
        ));
    }

    public function registerOrmConnections(): void
    {

    }
}
