<?php

return [
    'resources' => [
        'person' => [
            MiamiOH\PhpEmailService\Resources\EmailResourceProvider::class,
            \MiamiOH\PhpEmailService\V2\Resources\EmailResourceProvider::class
        ],

    ]
];